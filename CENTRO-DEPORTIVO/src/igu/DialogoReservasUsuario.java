package igu;

import java.awt.EventQueue;

import javax.swing.JDialog;
import javax.swing.JPanel;

import java.awt.BorderLayout;

import javax.swing.JTextArea;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.SQLException;

import logica.ManagerFinanzas;

public class DialogoReservasUsuario extends JDialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel panel;
	private JTextArea textArea;
	private JPanel panelDatos;
	private JLabel lblDatosDelUsuario;
	private JLabel lblReservas;
	private JPanel panelVerReservas;
	private int idUsuario;
	private JButton btnVerReservas;
	private DialogoReservasUsuario dialogoOriginal = this;
	private JPanel panelDatos2;
	private JPanel pnBotones;
	private JButton btnAlBanco;
	private ManagerFinanzas manager;

	/**
	 * Create the dialog.
	 */
	public DialogoReservasUsuario() {
		manager = new ManagerFinanzas();
		setBounds(100, 100, 975, 430);
		getContentPane().add(getPanel(), BorderLayout.CENTER);

	}

	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setLayout(new BorderLayout(0, 0));
			panel.add(getPanelDatos(), BorderLayout.WEST);
			panel.add(getPanelVerReservas(), BorderLayout.CENTER);
		}
		return panel;
	}
	private JTextArea getTextArea() {
		if (textArea == null) {
			textArea = new JTextArea();
			textArea.setEditable(false);
			textArea.setLineWrap(true);
			textArea.setWrapStyleWord(true);
		}
		return textArea;
	}
	private JPanel getPanelDatos() {
		if (panelDatos == null) {
			panelDatos = new JPanel();
			panelDatos.setLayout(new BorderLayout(0, 0));
			panelDatos.add(getLblDatosDelUsuario(), BorderLayout.NORTH);
			panelDatos.add(getPanel_1_1());
			panelDatos.add(getPanel_1_2(), BorderLayout.SOUTH);
		}
		return panelDatos;
	}
	private JLabel getLblDatosDelUsuario() {
		if (lblDatosDelUsuario == null) {
			lblDatosDelUsuario = new JLabel("Datos del usuario");
			lblDatosDelUsuario.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lblDatosDelUsuario;
	}
	private JLabel getLblReservas() {
		if (lblReservas == null) {
			lblReservas = new JLabel("Reservas");
			lblReservas.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lblReservas;
	}
	private JPanel getPanelVerReservas() {
		if (panelVerReservas == null) {
			panelVerReservas = new JPanel();
			panelVerReservas.setLayout(new BorderLayout(0, 0));
			panelVerReservas.add(getTextArea(), BorderLayout.CENTER);
			panelVerReservas.add(getLblReservas(), BorderLayout.NORTH);
		}
		return panelVerReservas;
	}
	private JButton getBtnVerReservas() {
		if (btnVerReservas == null) {
			btnVerReservas = new JButton("Ver reservas");
			btnVerReservas.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					DialogoEntradaUsuario dialogo2 = new DialogoEntradaUsuario(dialogoOriginal);
					dialogo2.setVisible(true);
				}
			});
		}
		return btnVerReservas;
	}
	
	
	private JPanel getPanel_1_1() {
		if (panelDatos2 == null) {
			panelDatos2 = new JPanel();
		}
		return panelDatos2;
	}
	private JPanel getPanel_1_2() {
		if (pnBotones == null) {
			pnBotones = new JPanel();
			pnBotones.add(getBtnVerReservas());
			pnBotones.add(getBtnAlBanco());
		}
		return pnBotones;
	}
	private JButton getBtnAlBanco() {
		if (btnAlBanco == null) {
			btnAlBanco = new JButton("Pasar a cuota");
			btnAlBanco.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						manager.liquidarReservas(1);
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			});
		}
		return btnAlBanco;
	}
	
	
	
	/*-----Metodos----------*/
	
	public void cambiarTextoReservas(StringBuilder sb){
		textArea.setText(sb.toString());
	}
}
