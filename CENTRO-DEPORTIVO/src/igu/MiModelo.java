package igu;

import javax.swing.table.DefaultTableModel;

public class MiModelo extends DefaultTableModel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MiModelo(Object[] numColumnas,int numFilas){
		super(numColumnas,numFilas);
	}
	
	public boolean isCellEditable(int numfila, int numcolumna){
		return false;
	}
}
