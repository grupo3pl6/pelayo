package igu;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.util.Date;
import java.sql.SQLException;
import java.text.ParseException;

import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import logica.Instalacion;
import logica.ManagerReservas;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import com.toedter.calendar.JCalendar;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JSpinner;

public class ReservasCentro extends JDialog {
	
	ManagerReservas mg;
	
	DefaultComboBoxModel<Instalacion> modeloCmbInstalaciones = new DefaultComboBoxModel<Instalacion>();
	private JPanel panel;
	private JComboBox cmbInstalaciones;
	private JCalendar calendar;
	private JPanel panel_1;
	private JLabel lblDesde;
	private JTextField txtDesde;
	private JLabel lblHasta;
	private JTextField txtHasta;
	private JRadioButton rdbtnReservarPorHoras;
	
	private JRadioButton rdbtnReservarDaCompleto;
	private ButtonGroup buttongroup;
	private JPanel panelBotones;
	private JButton btnReservar;
	private JButton btnCancelar;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			ReservasCentro dialog = new ReservasCentro();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 * @throws SQLException 
	 */
	public ReservasCentro() throws SQLException {
		setResizable(false);
		setTitle("CENTRO-DEPORTIVO: Reservas de Centro");
		setBounds(100, 100, 797, 480);
		getContentPane().setLayout(null);
		getContentPane().add(getPanel_1());
		mg = new ManagerReservas();
		
		cargarCmbInstalaciones();
		buttongroup = new ButtonGroup();
		buttongroup.add(rdbtnReservarDaCompleto);
		buttongroup.add(rdbtnReservarPorHoras);
		getContentPane().add(getPanelBotones());

	}
	
	private void cargarCmbInstalaciones() throws SQLException
	{
		mg.cargarCmbInstalaciones(modeloCmbInstalaciones);
		cmbInstalaciones.setModel(modeloCmbInstalaciones);
	}
	private JPanel getPanel_1() {
		if (panel == null) {
			panel = new JPanel();
			panel.setBounds(0, 30, 771, 331);
			panel.setLayout(null);
			panel.add(getCmbInstalaciones());
			panel.add(getCalendar());
			panel.add(getPanel_1_1());
		}
		return panel;
	}
	private JComboBox getCmbInstalaciones() {
		if (cmbInstalaciones == null) {
			cmbInstalaciones = new JComboBox();
			cmbInstalaciones.setBounds(10, 29, 199, 20);
		}
		return cmbInstalaciones;
	}
	private JCalendar getCalendar() {
		if (calendar == null) {
			calendar = new JCalendar();
			calendar.setBounds(219, 29, 184, 153);
		}
		return calendar;
	}
	private JPanel getPanel_1_1() {
		if (panel_1 == null) {
			panel_1 = new JPanel();
			panel_1.setBounds(446, 29, 284, 170);
			panel_1.setLayout(null);
			panel_1.add(getLblDesde());
			panel_1.add(getTxtDesde());
			panel_1.add(getLblHasta());
			panel_1.add(getTxtHasta());
			panel_1.add(getRdbtnReservarPorHoras());
			panel_1.add(getRdbtnReservarDaCompleto());
		}
		return panel_1;
	}
	private JLabel getLblDesde() {
		if (lblDesde == null) {
			lblDesde = new JLabel("Desde: ");
			lblDesde.setBounds(71, 52, 46, 14);
		}
		return lblDesde;
	}
	private JTextField getTxtDesde() {
		if (txtDesde == null) {
			txtDesde = new JTextField();
			txtDesde.setBounds(127, 46, 129, 20);
			txtDesde.setColumns(10);
		}
		return txtDesde;
	}
	private JLabel getLblHasta() {
		if (lblHasta == null) {
			lblHasta = new JLabel("Hasta:");
			lblHasta.setBounds(71, 89, 46, 14);
		}
		return lblHasta;
	}
	private JTextField getTxtHasta() {
		if (txtHasta == null) {
			txtHasta = new JTextField();
			txtHasta.setBounds(127, 86, 129, 20);
			txtHasta.setColumns(10);
		}
		return txtHasta;
	}
	private JRadioButton getRdbtnReservarPorHoras() {
		if (rdbtnReservarPorHoras == null) {
			rdbtnReservarPorHoras = new JRadioButton("Reservar por horas");
			rdbtnReservarPorHoras.setBounds(0, 7, 213, 23);
		}
		return rdbtnReservarPorHoras;
	}
	private JRadioButton getRdbtnReservarDaCompleto() {
		if (rdbtnReservarDaCompleto == null) {
			rdbtnReservarDaCompleto = new JRadioButton("Reservar d\u00EDa completo");
			rdbtnReservarDaCompleto.setBounds(0, 140, 213, 23);
		}
		return rdbtnReservarDaCompleto;
	}
	private JPanel getPanelBotones() {
		if (panelBotones == null) {
			panelBotones = new JPanel();
			panelBotones.setBounds(0, 372, 771, 58);
			panelBotones.setLayout(null);
			panelBotones.add(getBtnReservar());
			panelBotones.add(getBtnCancelar());
		}
		return panelBotones;
	}
	private JButton getBtnReservar() {
		if (btnReservar == null) {
			btnReservar = new JButton("Reservar");
			btnReservar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						hacerReserva();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			});
			btnReservar.setBounds(373, 11, 89, 23);
		}
		return btnReservar;
	}
	private JButton getBtnCancelar() {
		if (btnCancelar == null) {
			btnCancelar = new JButton("Cancelar");
			btnCancelar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					System.exit(0);
				}
			});
			btnCancelar.setBounds(472, 11, 89, 23);
		}
		return btnCancelar;
	}
	
	private void hacerReserva() throws SQLException, ParseException
	{
		
		if (rdbtnReservarDaCompleto.isSelected()){
			if (!(mg.instalacionReservadaDia((Instalacion) this.modeloCmbInstalaciones.getSelectedItem(), calendar.getDate())) && mg.fechaPosteriorAactual(calendar.getDate()))
			{mg.hacerReservaCentro((Instalacion) this.modeloCmbInstalaciones.getSelectedItem(), calendar.getDate());
			JOptionPane.showMessageDialog(null,
					"Reserva realizada", "Confirmacion",
					JOptionPane.INFORMATION_MESSAGE);
			cmbInstalaciones.requestFocus();
			}
		}
		else {
			if (txtDesde.getText().length() > 0 && txtHasta.getText().length() > 0)
			{
				if (!(mg.instalacionReservadaHoras((Instalacion) this.modeloCmbInstalaciones.getSelectedItem(), calendar.getDate(), txtDesde.getText(), txtHasta.getText())) && mg.fechaPosteriorAactual(calendar.getDate()))
				{mg.hacerReservaCentro((Instalacion) this.modeloCmbInstalaciones.getSelectedItem(), calendar.getDate(), txtDesde.getText(), txtHasta.getText());
				JOptionPane.showMessageDialog(null,
						"Reserva realizada", "Confirmacion",
						JOptionPane.INFORMATION_MESSAGE);
				cmbInstalaciones.requestFocus();
				}
			}
			else {
				JOptionPane.showMessageDialog(null,
						"Error: Introducir hora de inicio y fin de la reserva", "Error Message",
						JOptionPane.ERROR_MESSAGE);
				txtHasta.requestFocus();
			}
		}


	}
	
	private void limpiarFormulario()
	{
		txtDesde.setText(null);
		txtHasta.setText(null);
		cmbInstalaciones.setSelectedIndex(0);
		calendar.setDate(new Date());
	}
}
