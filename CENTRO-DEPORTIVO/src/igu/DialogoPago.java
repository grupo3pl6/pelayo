package igu;

import java.awt.EventQueue;

import javax.swing.JDialog;

import javax.swing.JOptionPane;
import javax.swing.JTextField;

import javax.swing.JLabel;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.sql.SQLException;

import logica.ManagerFinanzas;

public class DialogoPago extends JDialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel lblPagos;
	private JLabel lblIdReserva;
	private JTextField textFieldIDReserva;
	private JButton btnOk;
	private JButton btnCancelar;
	private ManagerFinanzas manager;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DialogoPago dialog = new DialogoPago();
					dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
					dialog.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the dialog.
	 */
	public DialogoPago() {
		setBounds(100, 100, 281, 189);
		getContentPane().setLayout(null);
		getContentPane().add(getLblPagos());
		getContentPane().add(getLblIdReserva());
		getContentPane().add(getTextFieldIDReserva());
		getContentPane().add(getBtnOk());
		getContentPane().add(getBtnCancelar());
		manager = new ManagerFinanzas();

	}
	private JLabel getLblPagos() {
		if (lblPagos == null) {
			lblPagos = new JLabel("Marcar reserva como pagada");
			lblPagos.setBounds(20, 11, 194, 14);
		}
		return lblPagos;
	}
	private JLabel getLblIdReserva() {
		if (lblIdReserva == null) {
			lblIdReserva = new JLabel("ID reserva:");
			lblIdReserva.setBounds(20, 36, 119, 14);
		}
		return lblIdReserva;
	}
	private JTextField getTextFieldIDReserva() {
		if (textFieldIDReserva == null) {
			textFieldIDReserva = new JTextField();
			textFieldIDReserva.setBounds(118, 36, 86, 20);
			textFieldIDReserva.setColumns(10);
		}
		return textFieldIDReserva;
	}
	private JButton getBtnOk() {
		if (btnOk == null) {
			btnOk = new JButton("OK");
			btnOk.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						manager.registrarPagoEfectivo(Integer.valueOf(textFieldIDReserva.getText()));
						manager.imprimirTicket(Integer.valueOf(textFieldIDReserva.getText()));
						
						JOptionPane.showMessageDialog(null, "Recibo generado");
					} catch (NumberFormatException | SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					dispose();
				}
			});
			btnOk.setBounds(20, 71, 89, 23);
		}
		return btnOk;
	}
	private JButton getBtnCancelar() {
		if (btnCancelar == null) {
			btnCancelar = new JButton("Cancelar");
			btnCancelar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					dispose();
				}
			});
			btnCancelar.setBounds(115, 71, 89, 23);
		}
		return btnCancelar;
	}
}
