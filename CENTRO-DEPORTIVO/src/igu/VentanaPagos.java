package igu;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.SQLException;

import logica.ManagerFinanzas;

import javax.swing.BoxLayout;

import java.awt.Dimension;

import javax.swing.JTextArea;
import javax.swing.SwingConstants;

public class VentanaPagos extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JPanel panelTop;
	private JLabel lblTitulo;
	private JButton btnPagada;
	private ManagerFinanzas manager;
	private JPanel panelWest;
	private JButton btnVerReservas;
	private JTextArea textAreaReservas;
	private JPanel panel;
	private JLabel lblReservas;
	private JButton btnVerReservasDe;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaPagos frame = new VentanaPagos();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VentanaPagos() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1093, 465);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		contentPane.add(getPanelTop(), BorderLayout.NORTH);
		contentPane.add(getPanel_1(), BorderLayout.WEST);
		contentPane.add(getPanel(), BorderLayout.CENTER);
		manager = new ManagerFinanzas();
	}

	private JPanel getPanelTop() {
		if (panelTop == null) {
			panelTop = new JPanel();
			panelTop.add(getLblTitulo());
		}
		return panelTop;
	}
	private JLabel getLblTitulo() {
		if (lblTitulo == null) {
			lblTitulo = new JLabel("Administraci\u00F3n Centro Deportivo");
		}
		return lblTitulo;
	}
	private JButton getBtnPagada() {
		if (btnPagada == null) {
			btnPagada = new JButton("Pago en efectivo");
			btnPagada.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					DialogoPago dialogo = new DialogoPago();
					dialogo.setVisible(true);
				}
			});
			btnPagada.setPreferredSize(new Dimension(150, 23));
			btnPagada.setMinimumSize(new Dimension(150, 23));
			btnPagada.setMaximumSize(new Dimension(150, 23));
		}
		return btnPagada;
	}
	private JPanel getPanel_1() {
		if (panelWest == null) {
			panelWest = new JPanel();
			panelWest.setLayout(new BoxLayout(panelWest, BoxLayout.Y_AXIS));
			panelWest.add(getBtnVerReservas());
			panelWest.add(getBtnPagada());
			panelWest.add(getBtnVerReservasDe());
		}
		return panelWest;
	}
	private JButton getBtnVerReservas() {
		if (btnVerReservas == null) {
			btnVerReservas = new JButton("Ver reservas");
			btnVerReservas.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						textAreaReservas.setText(manager.obtenerReservas().toString());
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
				}
			});
			btnVerReservas.setMinimumSize(new Dimension(150, 23));
			btnVerReservas.setMaximumSize(new Dimension(150, 23));
		}
		return btnVerReservas;
	}
	private JTextArea getTextAreaReservas() {
		if (textAreaReservas == null) {
			textAreaReservas = new JTextArea();
			textAreaReservas.setWrapStyleWord(true);
			textAreaReservas.setLineWrap(true);
			textAreaReservas.setEditable(false);
		}
		return textAreaReservas;
	}
	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setLayout(new BorderLayout(0, 0));
			panel.add(getTextAreaReservas());
			panel.add(getLblReservas(), BorderLayout.NORTH);
		}
		return panel;
	}
	private JLabel getLblReservas() {
		if (lblReservas == null) {
			lblReservas = new JLabel("Reservas");
			lblReservas.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lblReservas;
	}
	private JButton getBtnVerReservasDe() {
		if (btnVerReservasDe == null) {
			btnVerReservasDe = new JButton("Reservas usuario");
			btnVerReservasDe.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					DialogoReservasUsuario dialogoUsuario = new DialogoReservasUsuario();
					dialogoUsuario.setVisible(true);
				}
			});
			btnVerReservasDe.setPreferredSize(new Dimension(150, 23));
			btnVerReservasDe.setMinimumSize(new Dimension(150, 23));
			btnVerReservasDe.setMaximumSize(new Dimension(150, 23));
		}
		return btnVerReservasDe;
	}
}
