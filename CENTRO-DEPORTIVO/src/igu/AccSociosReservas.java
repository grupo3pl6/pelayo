package igu;

import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import com.toedter.calendar.JCalendar;

import logica.Instalacion;
import logica.ManagerReservas;
import logica.Socio;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.InputMethodListener;
import java.awt.event.InputMethodEvent;
import javax.swing.JTextArea;

public class AccSociosReservas extends JDialog {

/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

ManagerReservas mg;
	
	DefaultComboBoxModel<Instalacion> modeloCmbInstalaciones = new DefaultComboBoxModel<Instalacion>();
	DefaultComboBoxModel<Socio> modeloCmbSocios = new DefaultComboBoxModel<Socio>();
	private JPanel panel;
	private JComboBox cmbInstalaciones;
	private JCalendar calendar;
	private JPanel panel_1;
	private JLabel lblDesde;
	private JTextField txtDesde;
	private JLabel lblHasta;
	private JTextField txtHasta;
	private JRadioButton rdbtnReservarPorHoras;
	private ButtonGroup buttongroup;
	private ButtonGroup buttongroupFormaPago;
	private JPanel panelBotones;
	private JButton btnReservar;
	private JButton btnCancelar;
	private JComboBox cmbSocios;
	private JPanel panel_2;
	private JRadioButton rdbtnPagoEnEfectivo;
	private JRadioButton rdbtnPagoPorBanco;
	private JTextArea txtSocio;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			AccSociosReservas dialog = new AccSociosReservas();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 * @throws SQLException 
	 */
	public AccSociosReservas() throws SQLException {
		setTitle("CENTRO-DEPORTIVO: Acceso a Socios on-line");
		setBounds(100, 100, 797, 480);
		getContentPane().setLayout(null);
		getContentPane().add(getPanel_1());
		mg = new ManagerReservas();
		
		cargarCmbInstalaciones();
		cargarCmbSocios();
		
		//mostrar datos del usuario, bienvenida
		mostrarDatosUsuario();
		
		
		buttongroup = new ButtonGroup();
		buttongroup.add(rdbtnReservarPorHoras);
		
		buttongroupFormaPago = new ButtonGroup();
		buttongroupFormaPago.add(rdbtnPagoEnEfectivo);
		buttongroupFormaPago.add(rdbtnPagoPorBanco);
		getContentPane().add(getPanelBotones());
		
		try {
			comprobarBajaUsuario();
		} catch (HeadlessException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	private void cargarCmbInstalaciones() throws SQLException
	{
		mg.cargarCmbInstalaciones(modeloCmbInstalaciones);
		cmbInstalaciones.setModel(modeloCmbInstalaciones);
	}
	
	private void cargarCmbSocios() throws SQLException
	{
		mg.cargarCmbSocios(modeloCmbSocios);
		
		//para pruebas con un socio concreto, seleccionar en el cmb el socio 1
		Socio socio1 = new Socio(1, "Pepe", "Perez", 1, "111111", "01/01/2016", "19/11/2016");
		//Socio socio1 = new Socio(1, "Juan", "Lopez", 1, "222222", "01/01/2016", "19/10/2016");
		//Socio socio1 = new Socio(3, "Miguel", "Perez", 1, "333333", "01/01/2016", "");
		cmbSocios.setModel(modeloCmbSocios);
		modeloCmbSocios.setSelectedItem(socio1);
	}
	
	private JPanel getPanel_1() {
		if (panel == null) {
			panel = new JPanel();
			panel.setBounds(0, 11, 771, 355);
			panel.setLayout(null);
			panel.add(getTxtSocio());
			panel.add(getCmbInstalaciones());
			panel.add(getCalendar());
			panel.add(getPanel_1_1());
			panel.add(getCmbSocios());
			panel.add(getPanel_2());
		}
		return panel;
	}
	private JComboBox getCmbInstalaciones() {
		if (cmbInstalaciones == null) {
			cmbInstalaciones = new JComboBox();
			cmbInstalaciones.setToolTipText("Instalaciones");
			cmbInstalaciones.setBounds(10, 53, 199, 20);
		}
		return cmbInstalaciones;
	}
	private JCalendar getCalendar() {
		if (calendar == null) {
			calendar = new JCalendar();
			calendar.setToolTipText("Calendario");
			calendar.setBounds(232, 53, 184, 153);
		}
		return calendar;
	}
	private JPanel getPanel_1_1() {
		if (panel_1 == null) {
			panel_1 = new JPanel();
			panel_1.setBounds(446, 53, 284, 114);
			panel_1.setLayout(null);
			panel_1.add(getLblDesde());
			panel_1.add(getTxtDesde());
			panel_1.add(getLblHasta());
			panel_1.add(getTxtHasta());
			panel_1.add(getRdbtnReservarPorHoras());
		}
		return panel_1;
	}
	private JLabel getLblDesde() {
		if (lblDesde == null) {
			lblDesde = new JLabel("Desde: ");
			lblDesde.setBounds(71, 52, 46, 14);
		}
		return lblDesde;
	}
	private JTextField getTxtDesde() {
		if (txtDesde == null) {
			txtDesde = new JTextField();
			txtDesde.addFocusListener(new FocusAdapter() {
				
			});
			txtDesde.setToolTipText("Hora de inicio");
			txtDesde.setBounds(127, 46, 129, 20);
			txtDesde.setColumns(10);
		}
		return txtDesde;
	}
	private JLabel getLblHasta() {
		if (lblHasta == null) {
			lblHasta = new JLabel("Hasta:");
			lblHasta.setBounds(71, 89, 46, 14);
		}
		return lblHasta;
	}
	private JTextField getTxtHasta() {
		if (txtHasta == null) {
			txtHasta = new JTextField();
			txtHasta.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					
				}
			});
			txtHasta.addInputMethodListener(new InputMethodListener() {
				public void caretPositionChanged(InputMethodEvent arg0) {
				}
				public void inputMethodTextChanged(InputMethodEvent arg0) {
				}
			});
			txtHasta.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					if (txtDesde.getText() != "" && txtHasta.getText() != "")
						if ((Integer.parseInt(txtHasta.getText()) - Integer.parseInt(txtDesde.getText())) > 2)
						{
							System.out.println("Una reserva no puede durar mas de 2 horas");
							txtHasta.requestFocus();
							
						}
					
				}
			});
			txtHasta.setToolTipText("Hora de fin");
			txtHasta.setBounds(127, 86, 129, 20);
			txtHasta.setColumns(10);
		}
		return txtHasta;
	}
	private JRadioButton getRdbtnReservarPorHoras() {
		if (rdbtnReservarPorHoras == null) {
			rdbtnReservarPorHoras = new JRadioButton("Reservar por horas");
			rdbtnReservarPorHoras.setToolTipText("Realizar reserva por horas");
			rdbtnReservarPorHoras.setSelected(true);
			rdbtnReservarPorHoras.setBounds(0, 7, 213, 23);
		}
		return rdbtnReservarPorHoras;
	}
	private JPanel getPanelBotones() {
		if (panelBotones == null) {
			panelBotones = new JPanel();
			panelBotones.setBounds(0, 372, 771, 58);
			panelBotones.setLayout(null);
			panelBotones.add(getBtnReservar());
			panelBotones.add(getBtnCancelar());
		}
		return panelBotones;
	}
	private JButton getBtnReservar() {
		if (btnReservar == null) {
			btnReservar = new JButton("Reservar");
			btnReservar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						hacerReserva();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			});
			btnReservar.setBounds(373, 11, 89, 23);
		}
		return btnReservar;
	}
	private JButton getBtnCancelar() {
		if (btnCancelar == null) {
			btnCancelar = new JButton("Cancelar");
			btnCancelar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					System.exit(0);
				}
			});
			btnCancelar.setBounds(472, 11, 89, 23);
		}
		return btnCancelar;
	}
	
	private void hacerReserva() throws SQLException, ParseException
	{
		if (txtDesde.getText() != "" && txtHasta.getText() != "")
			if ((Integer.parseInt(txtHasta.getText()) - Integer.parseInt(txtDesde.getText())) > 2)
			{
				JOptionPane.showMessageDialog(null,
	                    "Error: Una reserva no puede durar mas de 2 horas", "Error Message",
	                    JOptionPane.ERROR_MESSAGE);
				txtHasta.requestFocus();
				
			}
			else {
		if (!(mg.instalacionReservadaDia((Instalacion) this.modeloCmbInstalaciones.getSelectedItem(), calendar.getDate())) && 
				!(mg.instalacionReservadaHoras((Instalacion) this.modeloCmbInstalaciones.getSelectedItem(), calendar.getDate(), txtDesde.getText(), txtHasta.getText())) &&
				!(mg.reservaSimultaneaSocio((Socio) this.modeloCmbSocios.getSelectedItem(), calendar.getDate(), txtDesde.getText(), txtHasta.getText())) && 
				mg.fechaReservaSocioCorrecta(calendar.getDate(), txtDesde.getText()) && mg.fechaPosteriorAactual(calendar.getDate()) && !mg.bajaMesEnCurso(((Socio) cmbSocios.getSelectedItem()).getFechaBaja()))
		{
			String formaPago;
			if (this.rdbtnPagoEnEfectivo.isSelected())
				formaPago = "Efectivo";
			else
				formaPago = "Banco";
			mg.hacerReservaSocio((Instalacion) this.modeloCmbInstalaciones.getSelectedItem(), (Socio) this.modeloCmbSocios.getSelectedItem(), calendar.getDate(), txtDesde.getText(), txtHasta.getText(), formaPago);
			
			JOptionPane.showMessageDialog(null,
                    "Reserva realizada", "Confirmacion",
                    JOptionPane.INFORMATION_MESSAGE);
			//limpiar formulario
			limpiarFormulario();
			cmbInstalaciones.requestFocus();
		}
		else
			limpiarFormulario();
			}
		
	}
	
	private void limpiarFormulario()
	{
		txtDesde.setText(null);
		txtHasta.setText(null);
		cmbInstalaciones.setSelectedIndex(0);
		// cmbSocios.setSelectedIndex(0); cargar el socio que esta haciendo la reserva
		calendar.setDate(new Date());
	}
	
	private JComboBox getCmbSocios() {
		if (cmbSocios == null) {
			cmbSocios = new JComboBox();
			cmbSocios.setVisible(false);
			cmbSocios.setEnabled(false);
			cmbSocios.setToolTipText("Socios");
			cmbSocios.setBounds(531, 296, 199, 20);
		}
		return cmbSocios;
	}
	private JPanel getPanel_2() {
		if (panel_2 == null) {
			panel_2 = new JPanel();
			panel_2.setBounds(446, 178, 284, 63);
			panel_2.setLayout(null);
			panel_2.add(getRdbtnPagoEnEfectivo());
			panel_2.add(getRdbtnPagoPorBanco());
		}
		return panel_2;
	}
	private JRadioButton getRdbtnPagoEnEfectivo() {
		if (rdbtnPagoEnEfectivo == null) {
			rdbtnPagoEnEfectivo = new JRadioButton("Pago en efectivo");
			rdbtnPagoEnEfectivo.setBounds(6, 19, 148, 23);
		}
		return rdbtnPagoEnEfectivo;
	}
	private JRadioButton getRdbtnPagoPorBanco() {
		if (rdbtnPagoPorBanco == null) {
			rdbtnPagoPorBanco = new JRadioButton("Pago por banco");
			rdbtnPagoPorBanco.setSelected(true);
			rdbtnPagoPorBanco.setBounds(156, 19, 122, 23);
		}
		return rdbtnPagoPorBanco;
	}
	private JTextArea getTxtSocio() {
		if (txtSocio == null) {
			txtSocio = new JTextArea();
			txtSocio.setBounds(10, 121, 199, 85);
		}
		return txtSocio;
	}
	
	private void mostrarDatosUsuario()
	{
		Socio socio = (Socio) cmbSocios.getSelectedItem();
		txtSocio.setText("Bienvenido: " + socio);
				
	}
	
	private void comprobarBajaUsuario() throws HeadlessException, ParseException
	{
		Socio socio = (Socio) cmbSocios.getSelectedItem();
		if ((socio.getFechaBaja()) != null && ( socio.getFechaBaja() != ""))
		{
			if (mg.bajaMesEnCurso(socio.getFechaBaja()))
			{
				//JOptionPane.showMessageDialog(null,
					//	"Error: La fecha de baja es para el mes en curso, no se pueden hacer reservas", "Error Message",
						//JOptionPane.ERROR_MESSAGE);
				limpiarFormulario();
				//System.exit(0);
			}
			else
			{
				rdbtnPagoPorBanco.setEnabled(false);
				rdbtnPagoEnEfectivo.setSelected(true);
			}
		}
		else
		{
			rdbtnPagoPorBanco.setEnabled(true);
			rdbtnPagoPorBanco.setSelected(true);
		}
	}
}
