package igu;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class CellRenderer extends DefaultTableCellRenderer{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
    public Component getTableCellRendererComponent(JTable jTable1,Object value,boolean selected, boolean focused, int row, int column){
    {
        setEnabled(jTable1 == null || jTable1.isEnabled());
        {
        	Font fuente = new Font("Arial",Font.CENTER_BASELINE,14);
        	jTable1.setFont(fuente);
	        if(String.valueOf(jTable1.getValueAt(row,2)).equals("Socio")){
	            setBackground(Color.CYAN);
	        }
	        else if(String.valueOf(jTable1.getValueAt(row,2)).equals("Centro")){
	        	setBackground(Color.magenta);
	        }
	        else if(String.valueOf(jTable1.getValueAt(row,2)).equals("Mi Reserva")){
	        	setBackground(Color.decode("#33FFCC"));
	        }
	        else if(String.valueOf(jTable1.getValueAt(row,2)).equals("Ocupado")){
	        	setBackground(Color.decode("#FF3300"));
	        }
	        else{
	            setBackground(Color.green);
	        }
        }
        super.getTableCellRendererComponent(jTable1, value, selected, focused, row, column);     
        return this;
    }
    }
}
