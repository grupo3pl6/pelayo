package igu;

import javax.swing.JDialog;
import javax.swing.JPanel;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.SQLException;

import logica.ManagerFinanzas;

public class DialogoEntradaUsuario extends JDialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel panel;
	private JButton btnOK;
	private JPanel pnBotones;
	private JButton btnCancelar;
	private JPanel panel1;
	private JLabel lblIntroduzcaElId;
	private JTextField textFieldID;
	private DialogoReservasUsuario dialogoAnterior = null;
	private ManagerFinanzas manager = null;

	/**
	 * Create the dialog.
	 * @param dialogoOriginal 
	 */
	public DialogoEntradaUsuario(DialogoReservasUsuario dialogoOriginal) {
		setTitle("Input de usuario");
		setResizable(false);
		setBounds(100, 100, 251, 150);
		getContentPane().add(getPanel(), BorderLayout.SOUTH);
		getContentPane().add(getPanel1(), BorderLayout.CENTER);
		dialogoAnterior = dialogoOriginal;
		manager = new ManagerFinanzas();

	}

	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setLayout(new BorderLayout(0, 0));
			panel.add(getPnBotones(), BorderLayout.SOUTH);
		}
		return panel;
	}
	private JButton getBtnOK() {
		if (btnOK == null) {
			btnOK = new JButton("OK");
			btnOK.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						dialogoAnterior.cambiarTextoReservas(manager.obtenerReservasPorUsuario(Integer.valueOf(textFieldID.getText())));
					} catch (NumberFormatException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					dispose();
				}
			});
		}
		return btnOK;
	}
	private JPanel getPnBotones() {
		if (pnBotones == null) {
			pnBotones = new JPanel();
			pnBotones.add(getBtnOK());
			pnBotones.add(getBtnCancelar());
		}
		return pnBotones;
	}
	private JButton getBtnCancelar() {
		if (btnCancelar == null) {
			btnCancelar = new JButton("Cancelar");
			btnCancelar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					dispose();
				}
			});
		}
		return btnCancelar;
	}
	private JPanel getPanel1() {
		if (panel1 == null) {
			panel1 = new JPanel();
			panel1.setLayout(null);
			panel1.add(getLblIntroduzcaElId());
			panel1.add(getTextFieldID());
		}
		return panel1;
	}
	private JLabel getLblIntroduzcaElId() {
		if (lblIntroduzcaElId == null) {
			lblIntroduzcaElId = new JLabel("Introduzca el ID de usuario");
			lblIntroduzcaElId.setBounds(10, 11, 162, 14);
		}
		return lblIntroduzcaElId;
	}
	private JTextField getTextFieldID() {
		if (textFieldID == null) {
			textFieldID = new JTextField();
			textFieldID.setBounds(10, 36, 86, 20);
			textFieldID.setColumns(10);
		}
		return textFieldID;
	}
}
