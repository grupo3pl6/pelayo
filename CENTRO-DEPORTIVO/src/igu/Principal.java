package igu;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.BoxLayout;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import logica.Usuario;

import javax.swing.JComboBox;

public class Principal extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPanePrincipal;
	private JMenuBar menuBar;
	private JMenu mnArchivo;
	private JPanel panel;
	private JButton btnDisponibilidadInstalaciones;
	private JButton btnReservasCentro;
	private JButton btnReservasSocio;
	private Usuario usuario;
	private JComboBox<Usuario> comboBox;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Principal frame = new Principal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Principal() {
		usuario = new Usuario("a", "a", "admin");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 637, 362);
		setJMenuBar(getMenuBar_1());
		contentPanePrincipal = new JPanel();
		contentPanePrincipal.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPanePrincipal);
		contentPanePrincipal.setLayout(null);
		contentPanePrincipal.add(getPanel());
	}
	private JMenuBar getMenuBar_1() {
		if (menuBar == null) {
			menuBar = new JMenuBar();
			menuBar.add(getMnArchivo());
		}
		return menuBar;
	}
	private JMenu getMnArchivo() {
		if (mnArchivo == null) {
			mnArchivo = new JMenu("Archivo");
		}
		return mnArchivo;
	}
	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setBounds(5, 5, 208, 292);
			panel.setLayout(null);
			panel.add(getBtnDisponibilidadInstalaciones());
			panel.add(getBtnReservasCentro());
			panel.add(getBtnReservasSocio());
			panel.add(getComboBox());
			
			
		}
		return panel;
	}
	private JButton getBtnDisponibilidadInstalaciones() {
		if (btnDisponibilidadInstalaciones == null) {
			btnDisponibilidadInstalaciones = new JButton("Disponibilidad Instalaciones");
			btnDisponibilidadInstalaciones.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
				}
			});
			btnDisponibilidadInstalaciones.setBounds(20, 30, 163, 56);
		}
		return btnDisponibilidadInstalaciones;
	}
	private JButton getBtnReservasCentro() {
		if (btnReservasCentro == null) {
			btnReservasCentro = new JButton("Reservas centro");
			btnReservasCentro.setBounds(20, 114, 163, 56);
		}
		return btnReservasCentro;
	}
	private JButton getBtnReservasSocio() {
		if (btnReservasSocio == null) {
			btnReservasSocio = new JButton("Reservas Socio");
			btnReservasSocio.setBounds(20, 192, 163, 56);
			if (usuario.getTipoUsuario() == "admin") btnReservasSocio.setEnabled(false);
		}
		return btnReservasSocio;
	}
	private JComboBox<Usuario> getComboBox() {
		if (comboBox == null) {
			comboBox = new JComboBox<>();
			comboBox.setBounds(20, 259, 163, 20);
			rellenarCombo();
		}
		return comboBox;
	}
	
	private void rellenarCombo(){
		String[] array = {"hola","adios","gkgk"};
		DefaultComboBoxModel<Usuario> modelo = new DefaultComboBoxModel<Usuario>();
		for(int i=0; i<array.length; i++){
			modelo.addElement(new Usuario("perico","a","a"));
		}
		comboBox.setModel(modelo);
	}
}
