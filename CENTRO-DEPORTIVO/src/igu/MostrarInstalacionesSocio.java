package igu;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;

import conexion.Conexion;
import logica.ManagerConsultas;
import logica.Reserva;

import javax.swing.JScrollPane;
import javax.swing.JComboBox;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import com.toedter.calendar.JDateChooser;
import javax.swing.JTextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MostrarInstalacionesSocio extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JComboBox<String> cbInstalaciones;
	private JLabel lblInstalacin;
	private JLabel lblFecha;
	private JButton btnFiltrar;
	private JTable table;
	
	ManagerConsultas manager;
	private String InstalacionSeleccionada;
	private String fechaSeleccionada;
	private int idsocio;
	private int idReserva;
	private int row;
	
	private String[] datos = {"IdReserva","Hora", "Actividad"};
	private String[] hora = {"00:00","01:00","02:00","03:00","04:00","05:00","06:00",
						  "07:00","08:00","09:00","10:00","11:00","12:00","13:00",
						  "14:00","15:00","16:00","17:00","18:00","19:00","20:00",
						  "21:00","22:00","23:00"};
	private String[] periodoTiempo = {"00:00 - 01:00","01:00 - 02:00","02:00 - 03:00",
							  "03:00 - 04:00","04:00 - 05:00","05:00 - 06:00",
							  "06:00 - 07:00","07:00 - 08:00","08:00 - 09:00",
							  "09:00 - 10:00","10:00 - 11:00","11:00 - 12:00",
							  "12:00 - 13:00","13:00 - 14:00","14:00 - 15:00",
							  "15:00 - 16:00","16:00 - 17:00","17:00 - 18:00",
							  "18:00 - 19:00","19:00 - 20:00","20:00 - 21:00",
							  "21:00 - 22:00","22:00 - 23:00","23:00 - 23:59"};
	private JDateChooser dateChooser;
	private JLabel lblIdSocio;
	private JTextField txtId;
	private JPanel panelOpciones;
	private JButton btnCancelarReserva;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			MostrarInstalacionesSocio dialog = new MostrarInstalacionesSocio();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public MostrarInstalacionesSocio() {
		setFont(new Font("Dialog", Font.BOLD, 15));
		setTitle("Disponibilidad de Instalaciones");
		setBounds(100, 100, 792, 483);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JPanel panelNorte = new JPanel();
			panelNorte.setBounds(5, 5, 766, 104);
			contentPanel.add(panelNorte);
			panelNorte.setLayout(null);
			panelNorte.add(getLblInstalacin());
			panelNorte.add(getCbInstalaciones());
			panelNorte.add(getLblFecha());
			panelNorte.add(getDateChooser());
			panelNorte.add(getBtnFiltrar());
			panelNorte.add(getLblIdSocio());
			panelNorte.add(getTxtId());
		}
		{
			JScrollPane scrollPane = new JScrollPane();
			scrollPane.setBounds(5, 110, 766, 265);
			contentPanel.add(scrollPane);
			scrollPane.setViewportView(getTable());
		}
		contentPanel.add(getPanelOpciones());
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton cancelButton = new JButton("Cerrar");
				cancelButton.setFont(new Font("Tahoma", Font.BOLD, 12));
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		setInstalacionSeleccionada((String)cbInstalaciones.getSelectedItem());
		tratarFecha();
		comprobarHora("18:09");
	}
	private JComboBox<String> getCbInstalaciones() {
		if (cbInstalaciones == null) {
			cbInstalaciones = new JComboBox<>();
			cbInstalaciones.setBounds(156, 60, 124, 21);
			cbInstalaciones.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					setInstalacionSeleccionada((String)cbInstalaciones.getSelectedItem());
					MiModelo modelo = new MiModelo(datos, 0);
					table.setModel(modelo);
					propiedadesTabla(table);
				}
			});
			cbInstalaciones.setFont(new Font("Tahoma", Font.PLAIN, 12));
			try {
				rellenarCombo(cbInstalaciones);
			} catch (SQLException e) {
				System.out.println("Error al rellenar el comboBoxInstalaciones");
				e.printStackTrace();
			}
		}
		return cbInstalaciones;
	}
	private JLabel getLblInstalacin() {
		if (lblInstalacin == null) {
			lblInstalacin = new JLabel("Instalaci\u00F3n");
			lblInstalacin.setBounds(44, 63, 105, 15);
			lblInstalacin.setFont(new Font("Tahoma", Font.BOLD, 12));
		}
		return lblInstalacin;
	}
	private JLabel getLblFecha() {
		if (lblFecha == null) {
			lblFecha = new JLabel("Fecha");
			lblFecha.setBounds(346, 63, 47, 15);
			lblFecha.setFont(new Font("Tahoma", Font.BOLD, 12));
		}
		return lblFecha;
	}
	private JButton getBtnFiltrar() {
		if (btnFiltrar == null) {
			btnFiltrar = new JButton("Filtrar");
			btnFiltrar.setBounds(625, 59, 89, 23);
			btnFiltrar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if(txtId.getText().equals("")){
						JOptionPane.showMessageDialog(null, "Debe indicar su identificador de socio");
					}
					else{
						try {
							fechaActual();
						} catch (ParseException e1) {
							System.out.println("Error al comprobar la fecha actual");
							e1.printStackTrace();
						}
						tratarFecha();
					}
				}
			});
			btnFiltrar.setFont(new Font("Tahoma", Font.BOLD, 12));
		}
		return btnFiltrar;
	}
	private JTable getTable() {
		if (table == null) {
			table = new JTable();
			table.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent arg0) {
					int fila = table.getSelectedRow();
					if(fila != -1){
						String tipo = (String) table.getValueAt(fila, 2);
						if(!tipo.equals("Instalaci�n Disponible")){
							btnCancelarReserva.setEnabled(true);
							String idReserva = (String) table.getValueAt(fila, 0);
							setIdReserva(Integer.parseInt(idReserva));
							setRow(fila);
						}
						
					}
					
				}
			});
			table.setFont(new Font("Tahoma", Font.PLAIN, 12));
			MiModelo modelo = new MiModelo(datos, 0);
			table.setModel(modelo);
			propiedadesTabla(table);
		}
		return table;
	}
	
	/**
	 * M�todo que rellena el comboBoxInstalaciones con las Instalaciones existentes
	 * en el centro.
	 * @param combo
	 * @throws SQLException
	 */
	private void rellenarCombo(JComboBox<String> combo) throws SQLException{
		Conexion conexion = new Conexion();
		DefaultComboBoxModel<String> modelo = new DefaultComboBoxModel<>();
		manager = new ManagerConsultas();
		ResultSet rs = manager.nombreInstalaciones(conexion);
		try {
			while(rs.next()){
				String nombre = rs.getString(1);
				modelo.addElement(nombre);
			}
			conexion.cerrarConexion();
		} catch (SQLException e) {
			System.out.println("Error en el while rellenarCombo()");
			e.printStackTrace();
		}
		combo.setModel(modelo);
	}
	
	/**
	 * M�todo que rellena la tabla de la reserva de instalaciones
	 * @param tabla
	 * @throws SQLException
	 */
	private void rellenarTabla(JTable tabla) throws SQLException {
		setIdsocio(Integer.parseInt(txtId.getText()));
		ArrayList<Reserva> reservas = prepararReservas();
		MiModelo modelo = new MiModelo(datos, 0);
		int i=0;
		int j=0;
		for(Reserva r: reservas){
			while(!hora[i].equals(r.getDesde()) && i < hora.length){
				String[] fila = new String[3];
				fila[0] = "";
				fila[1] = periodoTiempo[i];
				fila[2] = "Instalaci�n Disponible";
				modelo.addRow(fila);
				i++;
				j=i;
			}
			String[] fila = new String[3];
			fila[0] = String.valueOf(r.getId());
			fila[1] = r.getDesde()+" - "+r.getHasta();
			if(r.getIdCliente()==getIdsocio()){
				fila[2] = "Mi Reserva";
			}
			else{
				fila[2] = "Ocupado";
			}
			modelo.addRow(fila);
			i++;
			j=i;
		}
		while(j<hora.length){
			String[] fila = new String[3];
			fila[0] = "";
			fila[1] = periodoTiempo[j];
			fila[2] = "Instalaci�n Disponible";
			modelo.addRow(fila);
			j++;
		}
		tabla.setModel(modelo);
		propiedadesTabla(tabla);
	}
	
	/**
	 * M�todo en el que se modifican ciertas propiedades que la tabla trae por
	 * defecto.
	 * @param tabla
	 */
	private void propiedadesTabla(JTable tabla){
		//Solo queremos que se pueda seleccionar una fila.
		tabla.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		//No queremos que el usuario pueda alterar el orden de las columnas
		tabla.getTableHeader().setReorderingAllowed(false);
		//Fijamos el ancho de las columnas de la tabla
		//Columna "C�digo". La hacemos invisible.
		TableColumn columna = tabla.getColumn(datos[0]);
		columna.setMinWidth(0);
		columna.setMaxWidth(0);
		//Fuente de la cabecera de la JTable
		JTableHeader th = tabla.getTableHeader();
		Font fuente = new Font("Verdana", Font.ITALIC, 25); 
		th.setFont(fuente);
		th.setBackground(Color.orange);
		//Asignar color a las filas, seg�n el tipo de reserva
		table.setDefaultRenderer (Object.class, new CellRenderer());
	}
	
	/**
	 * M�todo que retorna un ArrayList<Reserva> con las reservas existentes para
	 * una Instalaci�n y para un d�a determinado.
	 * @return
	 * @throws SQLException
	 */
	private ArrayList<Reserva> prepararReservas() throws SQLException{
		Conexion conexion = new Conexion();
		tratarFecha();
		ArrayList<Reserva> reservas = new ArrayList<>();
		manager = new ManagerConsultas();
		ResultSet rs = manager.horasInstalacionReservadas(conexion, getInstalacionSeleccionada(), getFechaSeleccionada());
		try{
			while(rs.next()){
				reservas.add(new Reserva(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getInt(4), rs.getString(5),
						rs.getBoolean(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getBoolean(10),
						rs.getBoolean(11), rs.getBoolean(12), rs.getString(13), rs.getString(14)));
			}
			conexion.cerrarConexion();
			return reservas;
		}
		catch(SQLException e){
			System.out.println("Error en el while prepararReservas()");
			e.printStackTrace();
		}
		return null;
	}

	public String getInstalacionSeleccionada() {
		return InstalacionSeleccionada;
	}

	private void setInstalacionSeleccionada(String instalacionSeleccionada) {
		InstalacionSeleccionada = instalacionSeleccionada;
	}
	private JDateChooser getDateChooser() {
		if (dateChooser == null) {
			dateChooser = new JDateChooser("dd/MM/yyyy", "##/##/####", '_');	
			dateChooser.setBounds(403, 58, 137, 20);
			Date fecha = new Date();
			dateChooser.setDate(fecha);			
		}
		return dateChooser;
	}
	private void tratarFecha(){
		Date fecha = dateChooser.getDate();
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		setFechaSeleccionada(String.valueOf(formato.format(fecha)));
	}
	
	private void fechaActual() throws ParseException{
		Date fecha = new Date();
		Date fechac = dateChooser.getDate();
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		String date1 = formato.format(fecha);
		String date2 = formato.format(fechac);
		if(date2.compareTo(date1) < 0){
			JOptionPane.showMessageDialog(null, "Ha seleccionado una fecha que\n \tya se ha producido",
					"ERROR", JOptionPane.ERROR_MESSAGE);
			MiModelo modelo = new MiModelo(datos, 0);
			table.setModel(modelo);
			propiedadesTabla(table);
		}
		else{
			try {
				rellenarTabla(table);
			} catch (SQLException e) {
				System.out.println("Error al rellenar la tabla");
				e.printStackTrace();
			}
		}
	}
	public String getFechaSeleccionada() {
		return fechaSeleccionada;
	}
	private void setFechaSeleccionada(String fechaSeleccionada) {
		this.fechaSeleccionada = fechaSeleccionada;
	}
	private JLabel getLblIdSocio() {
		if (lblIdSocio == null) {
			lblIdSocio = new JLabel("Id Socio");
			lblIdSocio.setFont(new Font("Tahoma", Font.BOLD, 12));
			lblIdSocio.setBounds(44, 22, 83, 15);
		}
		return lblIdSocio;
	}
	private JTextField getTxtId() {
		if (txtId == null) {
			txtId = new JTextField();
			txtId.setFont(new Font("Tahoma", Font.PLAIN, 12));
			txtId.setBounds(156, 20, 124, 20);
			txtId.setColumns(10);
		}
		return txtId;
	}
	public int getIdsocio() {
		return idsocio;
	}
	private void setIdsocio(int idsocio) {
		this.idsocio = idsocio;
	}
	private JPanel getPanelOpciones() {
		if (panelOpciones == null) {
			panelOpciones = new JPanel();
			panelOpciones.setBounds(5, 376, 766, 35);
			panelOpciones.add(getBtnCancelarReserva());
		}
		return panelOpciones;
	}
	private JButton getBtnCancelarReserva() {
		if (btnCancelarReserva == null) {
			btnCancelarReserva = new JButton("Cancelar Reserva");
			btnCancelarReserva.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if(comprobarHora(hora[getRow()])){
						int result = JOptionPane.showConfirmDialog(jf(), "�Est� seguro de que desea cancelar su reserva?");
						if(result==0){
							manager = new ManagerConsultas();
							if(manager.cancelarReserva(getIdReserva())){
								JOptionPane.showMessageDialog(jf(), "Su reserva se ha cancelado con �xito");
							}
							else{
								JOptionPane.showMessageDialog(jf(), "Su reserva se ha cancelado con �xito");
							}
							try {
								rellenarTabla(table);
							} catch (SQLException e) {
								System.out.println(e);
								e.printStackTrace();
							}
						}
						btnCancelarReserva.setEnabled(false);
					}
					else{
						JOptionPane.showMessageDialog(jf(),"No es posible cancelar su reserva", "",JOptionPane.ERROR_MESSAGE);
						btnCancelarReserva.setEnabled(false);
					}
				}
			});
			btnCancelarReserva.setEnabled(false);
			btnCancelarReserva.setFont(new Font("Tahoma", Font.BOLD, 12));
		}
		return btnCancelarReserva;
	}
	
	private boolean comprobarHora(String hora){
		Date fecha = new Date();
		SimpleDateFormat formato = new SimpleDateFormat("HH:mm");
		String horaSistema = formato.format(fecha);
		String[] partes1 = hora.split(":");
		String[] partes2 = horaSistema.split(":");
		
		if(Integer.parseInt(partes1[0])-Integer.parseInt(partes2[0]) > 1){
			return true;
		}		
		else if(Integer.parseInt(partes1[0])-Integer.parseInt(partes2[0]) == 1){
			if(partes1[1].equals(partes2[1])){
				return true;
			}
		}
		return false;
	}
	
	private MostrarInstalacionesSocio jf(){
		return this;
	}

	public int getIdReserva() {
		return idReserva;
	}

	private void setIdReserva(int idReserva) {
		this.idReserva = idReserva;
	}

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}
}
