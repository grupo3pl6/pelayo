package igu;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;

import conexion.Conexion;
import logica.ManagerConsultas;
import logica.Reserva;

import javax.swing.JScrollPane;
import javax.swing.JComboBox;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import com.toedter.calendar.JDateChooser;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MostrarInstalacionesCentro extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JComboBox<String> cbInstalaciones;
	private JLabel lblInstalacin;
	private JLabel lblFecha;
	private JButton btnFiltrar;
	private JTable table;
	
	ManagerConsultas manager;
	private String InstalacionSeleccionada;
	private String fechaSeleccionada;
	private String[] datos = {"IdReserva","Hora", "Actividad"};
	private String[] hora = {"00:00","01:00","02:00","03:00","04:00","05:00","06:00",
						  "07:00","08:00","09:00","10:00","11:00","12:00","13:00",
						  "14:00","15:00","16:00","17:00","18:00","19:00","20:00",
						  "21:00","22:00","23:00"};
	private String[] periodoTiempo = {"00:00 - 01:00","01:00 - 02:00","02:00 - 03:00",
							  "03:00 - 04:00","04:00 - 05:00","05:00 - 06:00",
							  "06:00 - 07:00","07:00 - 08:00","08:00 - 09:00",
							  "09:00 - 10:00","10:00 - 11:00","11:00 - 12:00",
							  "12:00 - 13:00","13:00 - 14:00","14:00 - 15:00",
							  "15:00 - 16:00","16:00 - 17:00","17:00 - 18:00",
							  "18:00 - 19:00","19:00 - 20:00","20:00 - 21:00",
							  "21:00 - 22:00","22:00 - 23:00","23:00 - 23:59"};
	private JDateChooser dateChooser;
	private JPanel panel;
	private JButton btnLlegada;
	private JButton btnSalida;
	
	private String codigoReserva;
	private String desde;
	private String hasta;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			MostrarInstalacionesCentro dialog = new MostrarInstalacionesCentro();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public MostrarInstalacionesCentro() {
		setFont(new Font("Dialog", Font.BOLD, 15));
		setTitle("Disponibilidad de Instalaciones");
		setBounds(100, 100, 792, 483);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JPanel panelNorte = new JPanel();
			panelNorte.setBounds(5, 5, 766, 104);
			contentPanel.add(panelNorte);
			GridBagLayout gbl_panelNorte = new GridBagLayout();
			gbl_panelNorte.columnWidths = new int[]{44, 110, 129, 65, 52, 142, 56, 89, 0};
			gbl_panelNorte.rowHeights = new int[]{42, 23, 0};
			gbl_panelNorte.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
			gbl_panelNorte.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
			panelNorte.setLayout(gbl_panelNorte);
			GridBagConstraints gbc_lblInstalacin = new GridBagConstraints();
			gbc_lblInstalacin.anchor = GridBagConstraints.NORTH;
			gbc_lblInstalacin.fill = GridBagConstraints.HORIZONTAL;
			gbc_lblInstalacin.insets = new Insets(0, 0, 0, 5);
			gbc_lblInstalacin.gridx = 1;
			gbc_lblInstalacin.gridy = 1;
			panelNorte.add(getLblInstalacin(), gbc_lblInstalacin);
			GridBagConstraints gbc_cbInstalaciones = new GridBagConstraints();
			gbc_cbInstalaciones.anchor = GridBagConstraints.NORTH;
			gbc_cbInstalaciones.fill = GridBagConstraints.HORIZONTAL;
			gbc_cbInstalaciones.insets = new Insets(0, 0, 0, 5);
			gbc_cbInstalaciones.gridx = 2;
			gbc_cbInstalaciones.gridy = 1;
			panelNorte.add(getCbInstalaciones(), gbc_cbInstalaciones);
			GridBagConstraints gbc_lblFecha = new GridBagConstraints();
			gbc_lblFecha.anchor = GridBagConstraints.NORTH;
			gbc_lblFecha.fill = GridBagConstraints.HORIZONTAL;
			gbc_lblFecha.insets = new Insets(0, 0, 0, 5);
			gbc_lblFecha.gridx = 4;
			gbc_lblFecha.gridy = 1;
			panelNorte.add(getLblFecha(), gbc_lblFecha);
			GridBagConstraints gbc_dateChooser = new GridBagConstraints();
			gbc_dateChooser.anchor = GridBagConstraints.NORTH;
			gbc_dateChooser.fill = GridBagConstraints.HORIZONTAL;
			gbc_dateChooser.insets = new Insets(0, 0, 0, 5);
			gbc_dateChooser.gridx = 5;
			gbc_dateChooser.gridy = 1;
			panelNorte.add(getDateChooser(), gbc_dateChooser);
			GridBagConstraints gbc_btnFiltrar = new GridBagConstraints();
			gbc_btnFiltrar.anchor = GridBagConstraints.NORTH;
			gbc_btnFiltrar.fill = GridBagConstraints.HORIZONTAL;
			gbc_btnFiltrar.gridx = 7;
			gbc_btnFiltrar.gridy = 1;
			panelNorte.add(getBtnFiltrar(), gbc_btnFiltrar);
		}
		{
			JScrollPane scrollPane = new JScrollPane();
			scrollPane.setBounds(5, 110, 766, 249);
			contentPanel.add(scrollPane);
			scrollPane.setViewportView(getTable());
		}
		contentPanel.add(getPanel());
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton cancelButton = new JButton("Cerrar");
				cancelButton.setFont(new Font("Tahoma", Font.BOLD, 12));
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		setInstalacionSeleccionada((String)cbInstalaciones.getSelectedItem());
		tratarFecha();
	}
	private JComboBox<String> getCbInstalaciones() {
		if (cbInstalaciones == null) {
			cbInstalaciones = new JComboBox<>();
			cbInstalaciones.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					setInstalacionSeleccionada((String)cbInstalaciones.getSelectedItem());
					MiModelo modelo = new MiModelo(datos, 0);
					table.setModel(modelo);
					propiedadesTabla(table);
				}
			});
			cbInstalaciones.setFont(new Font("Tahoma", Font.PLAIN, 12));
			try {
				rellenarCombo(cbInstalaciones);
			} catch (SQLException e) {
				System.out.println("Error al rellenar el comboBoxInstalaciones");
				e.printStackTrace();
			}
		}
		return cbInstalaciones;
	}
	private JLabel getLblInstalacin() {
		if (lblInstalacin == null) {
			lblInstalacin = new JLabel("Instalaci\u00F3n");
			lblInstalacin.setFont(new Font("Tahoma", Font.BOLD, 12));
		}
		return lblInstalacin;
	}
	private JLabel getLblFecha() {
		if (lblFecha == null) {
			lblFecha = new JLabel("Fecha");
			lblFecha.setFont(new Font("Tahoma", Font.BOLD, 12));
		}
		return lblFecha;
	}
	private JButton getBtnFiltrar() {
		if (btnFiltrar == null) {
			btnFiltrar = new JButton("Filtrar");
			btnFiltrar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					try {
						fechaActual();
					} catch (ParseException e1) {
						System.out.println("Error al comprobar la fecha actual");
						e1.printStackTrace();
					}
					tratarFecha();
				}
			});
			btnFiltrar.setFont(new Font("Tahoma", Font.BOLD, 12));
		}
		return btnFiltrar;
	}
	private JTable getTable() {
		if (table == null) {
			table = new JTable();
			table.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent arg0) {
					int fila = table.getSelectedRow();
					if(fila != -1){
						String codigo = (String) table.getValueAt(fila, 0);
						String tipo = (String) table.getValueAt(fila, 2);
						int result = 23;
						try {
							setCodigoReserva(codigo);
							result = desdeHasta();
						} catch (SQLException e) {
							e.printStackTrace();
						}
						if(!tipo.equals("Instalaci�n Disponible")){
							if(result==0){
								btnLlegada.setEnabled(true);
							}
							else if(result==1){
								JOptionPane.showMessageDialog(jf(),"Imposible modificar hasta llegar a ese dia.");
							}
							else{
								JOptionPane.showMessageDialog(jf(), "La hora de la reserva ya ha expirado");
								btnLlegada.setEnabled(false);
								btnSalida.setEnabled(false);
							}
						}
					}
				}
			});
			table.setFont(new Font("Tahoma", Font.PLAIN, 12));
			MiModelo modelo = new MiModelo(datos, 0);
			table.setModel(modelo);
			propiedadesTabla(table);
		}
		return table;
	}
	
	/**
	 * M�todo que rellena el comboBoxInstalaciones con las Instalaciones existentes
	 * en el centro.
	 * @param combo
	 * @throws SQLException
	 */
	private void rellenarCombo(JComboBox<String> combo) throws SQLException{
		Conexion conexion = new Conexion();
		DefaultComboBoxModel<String> modelo = new DefaultComboBoxModel<>();
		manager = new ManagerConsultas();
		ResultSet rs = manager.nombreInstalaciones(conexion);
		try {
			while(rs.next()){
				String nombre = rs.getString(1);
				modelo.addElement(nombre);
			}
			conexion.cerrarConexion();
		} catch (SQLException e) {
			System.out.println("Error en el while rellenarCombo()");
			e.printStackTrace();
		}
		combo.setModel(modelo);
	}
	
	/**
	 * M�todo que rellena la tabla de la reserva de instalaciones
	 * @param tabla
	 * @throws SQLException
	 */
	private void rellenarTabla(JTable tabla) throws SQLException {
		ArrayList<Reserva> reservas = prepararReservas();
		MiModelo modelo = new MiModelo(datos, 0);
		int i=0;
		int j=0;
		for(Reserva r: reservas){
			while(!hora[i].equals(r.getDesde()) && i < hora.length){
				String[] fila = new String[3];
				fila[0] = "";
				fila[1] = periodoTiempo[i];
				fila[2] = "Instalaci�n Disponible";
				modelo.addRow(fila);
				i++;
				j=i;
			}
			String[] fila = new String[3];
			fila[0] = String.valueOf(r.getId());
			fila[1] = periodoTiempo[i];//r.getDesde()+" - "+r.getHasta();
			fila[2] = r.getTipoReserva();
			modelo.addRow(fila);
			i++;
			j=i;
		}
		while(j<hora.length){
			String[] fila = new String[3];
			fila[0] = "";
			fila[1] = periodoTiempo[j];
			fila[2] = "Instalaci�n Disponible";
			modelo.addRow(fila);
			j++;
		}
		tabla.setModel(modelo);
		propiedadesTabla(tabla);
	}
	
	/**
	 * M�todo en el que se modifican ciertas propiedades que la tabla trae por
	 * defecto.
	 * @param tabla
	 */
	private void propiedadesTabla(JTable tabla){
		//Solo queremos que se pueda seleccionar una fila.
		tabla.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		//No queremos que el usuario pueda alterar el orden de las columnas
		tabla.getTableHeader().setReorderingAllowed(false);
		//Fijamos el ancho de las columnas de la tabla
		//Columna "C�digo". La hacemos invisible.
		TableColumn columna = tabla.getColumn(datos[0]);
		columna.setMinWidth(0);
		columna.setMaxWidth(0);
		//Fuente de la cabecera de la JTable
		JTableHeader th = tabla.getTableHeader();
		Font fuente = new Font("Verdana", Font.ITALIC, 25); 
		th.setFont(fuente);
		th.setBackground(Color.orange);
		//Asignar color a las filas, seg�n el tipo de reserva
		table.setDefaultRenderer (Object.class, new CellRenderer());
	}
	
	/**
	 * M�todo que retorna un ArrayList<Reserva> con las reservas existentes para
	 * una Instalaci�n y para un d�a determinado.
	 * @return
	 * @throws SQLException
	 */
	private ArrayList<Reserva> prepararReservas() throws SQLException{
		Conexion conexion = new Conexion();
		tratarFecha();
		ArrayList<Reserva> reservas = new ArrayList<>();
		manager = new ManagerConsultas();
		ResultSet rs = manager.horasInstalacionReservadas(conexion, getInstalacionSeleccionada(), getFechaSeleccionada());
		try{
			while(rs.next()){
				reservas.add(new Reserva(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getInt(4), rs.getString(5),
						rs.getBoolean(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getBoolean(10),
						rs.getBoolean(11), rs.getBoolean(12), rs.getString(13), rs.getString(14)));
			}
			conexion.cerrarConexion();
			return reservas;
		}
		catch(SQLException e){
			System.out.println("Error en el while prepararReservas()");
			e.printStackTrace();
		}
		return null;
	}

	public String getInstalacionSeleccionada() {
		return InstalacionSeleccionada;
	}

	private void setInstalacionSeleccionada(String instalacionSeleccionada) {
		InstalacionSeleccionada = instalacionSeleccionada;
	}
	private JDateChooser getDateChooser() {
		if (dateChooser == null) {
			dateChooser = new JDateChooser("dd/MM/yyyy", "##/##/####", '_');	
			Date fecha = new Date();
			dateChooser.setDate(fecha);			
		}
		return dateChooser;
	}
	private void tratarFecha(){
		Date fecha = dateChooser.getDate();
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		setFechaSeleccionada(String.valueOf(formato.format(fecha)));
	}
	
	private void fechaActual() throws ParseException{
		Date fecha = new Date();
		Date fechac = dateChooser.getDate();
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		String date1 = formato.format(fecha);
		String date2 = formato.format(fechac);
		if(date2.compareTo(date1) < 0){
			JOptionPane.showMessageDialog(null, "Ha seleccionado una fecha que\n \tya se ha producido",
					"ERROR", JOptionPane.ERROR_MESSAGE);
			MiModelo modelo = new MiModelo(datos, 0);
			table.setModel(modelo);
			propiedadesTabla(table);
		}
		else{
			try {
				rellenarTabla(table);
			} catch (SQLException e) {
				System.out.println("Error al rellenar la tabla");
				e.printStackTrace();
			}
		}
	}
	public String getFechaSeleccionada() {
		return fechaSeleccionada;
	}
	private void setFechaSeleccionada(String fechaSeleccionada) {
		this.fechaSeleccionada = fechaSeleccionada;
	}
	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setBounds(5, 361, 766, 45);
			panel.setLayout(null);
			panel.add(getBtnLlegada());
			panel.add(getBtnSalida());
		}
		return panel;
	}
	private JButton getBtnLlegada(){
		if (btnLlegada == null) {
			btnLlegada = new JButton("Llegada");
			btnLlegada.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					String hora = JOptionPane.showInputDialog("Introducir Hora de Llegada");
					if(hora!=null && !hora.equals("")){
						manager = new ManagerConsultas();
						if(manager.actualizarHoraLlegada(Integer.parseInt(getCodigoReserva()), hora)==1){
							JOptionPane.showMessageDialog(jf(), "La hora se ha insertado correctamente.");
							btnLlegada.setEnabled(false);
							btnSalida.setEnabled(true);
						}
						else{
							JOptionPane.showMessageDialog(jf(), "No se ha podido insertar la hora.");
						}
					}
					else{
						JOptionPane.showMessageDialog(jf(), "Introduzca una hora valida.");
					}
				}
			});
			btnLlegada.setEnabled(false);
			btnLlegada.setFont(new Font("Tahoma", Font.BOLD, 12));
			btnLlegada.setBounds(136, 11, 135, 23);
		}
		return btnLlegada;
	}
	private JButton getBtnSalida() {
		if (btnSalida == null) {
			btnSalida = new JButton("Salida");
			btnSalida.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					String hora = JOptionPane.showInputDialog("Introducir Hora de Salida");
					if(hora!=null && !hora.equals("")){
						manager = new ManagerConsultas();
						if(manager.actualizarHoraSalida(Integer.parseInt(getCodigoReserva()), hora)==1){
							JOptionPane.showMessageDialog(jf(), "La hora se ha insertado correctamente.");
							btnSalida.setEnabled(false);
						}
						else{
							JOptionPane.showMessageDialog(jf(), "No se ha podido insertar la hora.");
						}
					}
					else{
						JOptionPane.showMessageDialog(jf(), "Introduzca una hora valida.");
					}
				}
			});
			btnSalida.setEnabled(false);
			btnSalida.setFont(new Font("Tahoma", Font.BOLD, 12));
			btnSalida.setBounds(535, 11, 135, 23);
		}
		return btnSalida;
	}
	public String getCodigoReserva() {
		return codigoReserva;
	}
	private void setCodigoReserva(String codigoReserva) {
		this.codigoReserva = codigoReserva;
	}
	public String getDesde() {
		return desde;
	}
	public void setDesde(String desde) {
		this.desde = desde;
	}
	public String getHasta() {
		return hasta;
	}
	public void setHasta(String hasta) {
		this.hasta = hasta;
	}
	
	private int desdeHasta() throws SQLException{
		String dia = null;
		Date hora = new Date();
		SimpleDateFormat formato = new SimpleDateFormat("HH:mm");
		String date = formato.format(hora);
		Date fecha = new Date();
		SimpleDateFormat f = new SimpleDateFormat("dd/mm/yy");
		String cuando = f.format(fecha);
		Conexion conexion = new Conexion();
		manager = new ManagerConsultas();
		ResultSet rs = manager.horas(conexion, Integer.parseInt(getCodigoReserva()));
		try {
			while(rs.next()){
				desde = rs.getString(1);
				hasta = rs.getString(2);
				dia = rs.getString(3);
			}
			conexion.cerrarConexion();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		if(dia.compareTo(cuando) > 0){
			return 1;
		}
		if(desde.compareTo(date) < 0){
			return -1;
		}
		return 0;
	}
	
	private MostrarInstalacionesCentro jf(){
		return this;
	}
}
