package logica;

public class Instalacion {
	int id;
	String nombreInstalacion;
	int idCentro;
	
	public Instalacion(int id, String nombreInstalacion, int idCentro)
	{
		this.id = id;
		this.nombreInstalacion = nombreInstalacion;
		this.idCentro = idCentro;
	}

	@Override
	public String toString() {
		return nombreInstalacion;
	}

	public int getId() {
		return id;
	}

	public String getNombreInstalacion() {
		return nombreInstalacion;
	}

	public int getIdCentro() {
		return idCentro;
	}


}
