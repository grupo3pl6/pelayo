package logica;

public class Usuario {
	
	private String idUsuario;
	private String password;
	private String tipoUsuario;
	
	public Usuario(String idUsuario, String password, String tipoUsuario) {
		super();
		this.idUsuario = idUsuario;
		this.password = password;
		this.tipoUsuario = tipoUsuario;
	}
	
	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getTipoUsuario() {
		return tipoUsuario;
	}
	public void setTipoUsuario(String tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	@Override
	public String toString() {
		return idUsuario;
	}
	
	
	

}
