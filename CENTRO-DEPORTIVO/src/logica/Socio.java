package logica;

import java.util.Date;

public class Socio {
	private String nombreSocio;
	private String apellidoSocio;
	private int idCentro;
	private String dni;
	private String fechaAlta;
	private String FechaBaja;
	private int id;
	
	
	public Socio(int idSocio, String nombre, String apellido, int idCentro, String dni, String fechaAlta, String fechaBaja)
	{
		this.id = idSocio;
		this.nombreSocio = nombre;
		this.apellidoSocio = apellido;
		this.idCentro = idCentro;
		this.dni = dni;
		this.fechaAlta = fechaAlta;
		this.FechaBaja = fechaBaja;
	}
	
	public int getId() {
		return id;
	}
	
	public String getNombreSocio() {
		return nombreSocio;
	}
	public String getApellidoSocio() {
		return apellidoSocio;
	}
	public int getIdCentro() {
		return idCentro;
	}
	public String getDni() {
		return dni;
	}
	public String getFechaAlta() {
		return fechaAlta;
	}
	public String getFechaBaja() {
		return FechaBaja;
	}
	@Override
	public String toString() {
		return dni + " " + nombreSocio + " " + apellidoSocio;
	}
	
	
}
