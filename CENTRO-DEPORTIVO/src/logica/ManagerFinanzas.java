package logica;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import conexion.Conexion;

public class ManagerFinanzas {

	private Conexion conexion;
	private ResultSet rs;

	public StringBuilder obtenerReservas() throws SQLException {
		conexion = new Conexion();
		StringBuilder sb = new StringBuilder("");
		String consulta = "SELECT * FROM Reservas";
		rs = conexion.ejecutarConsulta(consulta);

		formatoReservas(sb);
		
		conexion.cerrarConexion();
		System.out.println(sb);
		return sb;
		}
	
	public StringBuilder obtenerReservasPorUsuario(int idUsuario) throws SQLException{
		conexion = new Conexion();
		StringBuilder sb = new StringBuilder("");
		String consulta = "SELECT * FROM Reservas WHERE IdCliente = " + idUsuario;
		rs = conexion.ejecutarConsulta(consulta);

		formatoReservas(sb);
		
		conexion.cerrarConexion();
		System.out.println(sb);
		return sb;
		}


	private void formatoReservas(StringBuilder sb) throws SQLException {
		ResultSetMetaData rsmd = rs.getMetaData();
		int columnsNumber = rsmd.getColumnCount();
		while (rs.next()) {
			for (int i = 1; i <= columnsNumber; i++) {
				String columnName = rsmd.getColumnName(i);
				String columnValue = rs.getString(i);
				
				//Inicio ticket
				if (columnName.equals("Id")) sb.append("ID Reserva: " + columnValue);
				else if (columnName.equals("FechaReserva")) sb.append(" | Fecha de la reserva: " + columnValue);
				else if (columnName.equals("IdInstalacion")) sb.append(" | ID Instalacion: " + columnValue);
				else if (columnName.equals("IdCliente")) sb.append(" | ID Cliente: " + columnValue);
				else if (columnName.equals("TipoReserva")) sb.append(" | Tipo de reserva: " + columnValue);
				else if (columnName.equals("Ocupada")) {
					sb.append(" | Instalacion ocupada: ");
					if ((columnValue).equals("TRUE"))
						sb.append("SI");
					else
						sb.append("NO");
				}
				else if (columnName.equals("Desde")) sb.append(" | Desde: " + columnValue);
				else if (columnName.equals("Hasta")) sb.append(" | Hasta: " + columnValue);
				else if (columnName.equals("DiaCompleto")) {
					sb.append(" | Dia completo: ");
					if ((columnValue) == "TRUE")
						sb.append("SI");
					else
						sb.append("NO");
				}
				else if (columnName.equals("HoraLlegada")) sb.append(" | Hora de llegada: " + columnValue);
				else if (columnName.equals("HoraSalida")) sb.append(" | Hora de salida: " + columnValue);
				else if (columnName.equals("Pagada")) {
					sb.append(" | Reserva pagada: ");
					if ((columnValue) == "TRUE")
						sb.append("SI");
					else
						sb.append("NO");
				}
				else if (columnName.equals("Cancelada")) {
					sb.append(" | Reserva cancelada: ");
					if ((columnValue) == "TRUE")
						sb.append("SI");
					else
						sb.append("NO");
				}
				else if (columnName.equals("FormaPago")) sb.append(" | Forma de pago: " + columnValue);
				
				}
			sb.append("\n");
			}
	}

	public void registrarPagoEfectivo(int idReserva) throws SQLException {
		Connection conexion = DriverManager.getConnection("jdbc:ucanaccess://BD\\IPSCentro1.mdb");
		Statement sentencia = conexion.createStatement();
		sentencia.executeUpdate("UPDATE RESERVAS SET PAGADA = TRUE WHERE Id = " + idReserva);
		conexion.close();
	}
	
	public void registrarReservaOcupada(int idReserva, String horaEntrada) throws SQLException{
		Connection conexion = DriverManager
				.getConnection("jdbc:ucanaccess://BD\\IPSCentro1.mdb");
		Statement sentencia = conexion.createStatement();
		sentencia.executeUpdate("UPDATE RESERVAS SET HORAENTRADA = " + horaEntrada + " WHERE Id = " + idReserva);
		conexion.close();
	}
	
	public void registrarReservaDesocupada(int idReserva, String horaSalida) throws SQLException{
		Connection conexion = DriverManager
				.getConnection("jdbc:ucanaccess://BD\\IPSCentro1.mdb");
		Statement sentencia = conexion.createStatement();
		sentencia.executeUpdate("UPDATE RESERVAS SET HORASALIDA = " + horaSalida + " WHERE Id = " + idReserva);
		conexion.close();
	}

	public StringBuilder obtenerReservaParaTicket(int idReserva) throws SQLException {
		conexion = new Conexion();
		StringBuilder sb = new StringBuilder("");
		String consulta = "SELECT * FROM Reservas WHERE ID = " + idReserva;

		rs = conexion.ejecutarConsulta(consulta);

		ResultSetMetaData rsmd = rs.getMetaData();
		int columnsNumber = rsmd.getColumnCount();
		while (rs.next()) {
			for (int i = 1; i <= columnsNumber; i++) {
				if (i > 1)
					sb.append("\n");
				String columnName = rsmd.getColumnName(i);
				String columnValue = rs.getString(i);
				//Inicio ticket
				if (columnName.equals("Id")) sb.append("ID Reserva: " + columnValue);
				else if (columnName.equals("FechaReserva")) sb.append("Fecha de la reserva: " + columnValue);
				else if (columnName.equals("IdInstalacion")) sb.append("ID Instalacion: " + columnValue);
				else if (columnName.equals("IdCliente")) sb.append("ID Cliente: " + columnValue);
				else if (columnName.equals("TipoReserva")) sb.append("Tipo de reserva: " + columnValue);
				else if (columnName.equals("Ocupada")) { sb.append("Instalacion ocupada: ");
					if ((columnValue).equals("TRUE"))
						sb.append("SI");
					else
						sb.append("FALSE");
				} 
				else if (columnName.equals("HoraSalida")) sb.append("Hora de salida: " + columnValue);
				else if (columnName.equals("Pagada")) { sb.append("Reserva pagada: ");
					if ((columnValue) == "TRUE")
						sb.append("SI");
					else
						sb.append("FALSE");
				//Fin del ticket
				}

			}
			sb.append("");
			sb.append("\n");
			sb.append("GRACIAS POR SU RESERVA");
		}
		conexion.cerrarConexion();
		return sb;
	}

	/**
	 * Imprime ticket
	 * 
	 * @param idReserva
	 * @throws IOException
	 * @throws SQLException
	 */
	public void imprimirTicket(int idReserva) throws IOException, SQLException {
		File file = new File("reserva" + idReserva + ".txt");

		// crear archivo
		if (!file.exists()) {
			file.createNewFile();
		}

		FileWriter fw = new FileWriter(file.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);
		bw.write("TICKET RESERVA CENTRO DEPORTIVO \n");
		bw.write(obtenerReservaParaTicket(idReserva).toString());
		bw.write("");
		bw.close();

		System.out.println("Recibo generado");
	}
	
	/**
	 * Incrementa la cuota mensual del socio para el siguiente mes con el importe de las reservas de instalaciones pendientes de liquidar.
	 * Se hará en caso de que el socio haya seleccionado el pago a través de su cuota mensual o que no haya registrado el pago correspondiente en efectivo.
	 * Las reservas que se pasan a la cuota serán desde el 20 del mes anterior al 19 de el mes actual. Se acumulan a la cuota del mes siguiente.
	 * @param idUsuario ID del usuario/cliente 
	 * @throws SQLException 
	 */
	public void liquidarReservas(int idUsuario) throws SQLException {
		Connection conexion = DriverManager.getConnection("jdbc:ucanaccess://BD\\IPSCentro1.mdb");
		Statement sentencia = conexion.createStatement(); 
		ResultSet rs =  sentencia.executeQuery("SELECT * FROM RESERVAS WHERE IdCliente = " + idUsuario +"  AND ((FormaPago = 'Banco') OR (FormaPago = 'Efectivo' AND Pagada = 'FALSE') )");
		int cuotaParaAñadir = 0;
		
		ResultSetMetaData rsmd = rs.getMetaData();
		int columnsNumber = rsmd.getColumnCount();
		while (rs.next()) {
			for (int i = 1; i <= columnsNumber; i++) {
				String columnName = rsmd.getColumnName(i);
				String columnValue = rs.getString(i);
				
				if (columnName.equals("IdInstalacion")){
					ResultSet rsTarifas = sentencia.executeQuery("SELECT * FROM TarifasInstalaciones WHERE IdInstalacion = " + columnValue);
					ResultSetMetaData rsmdTarifas = rsTarifas.getMetaData();
					int columnsNumberTarifas = rsmdTarifas.getColumnCount();
					
					while(rsTarifas.next()){
						for (int j = 1; j <= columnsNumberTarifas; j++){
							String columnNameTarifas = rsmdTarifas.getColumnName(j);
							String columnValueTarifas = rsTarifas.getString(j);
							
							if (columnNameTarifas.equals("ImporteHora")){
								cuotaParaAñadir += Integer.valueOf(columnValueTarifas);
							}
						}
					}
				}			
			}
			System.out.println();
		}
		System.out.println("CUOTA FINAL A AÑADIR: " + cuotaParaAñadir);
		actualizarCuotaMensual(idUsuario,cuotaParaAñadir);
		conexion.close();
		
	}

	private void actualizarCuotaMensual(int idUsuario, int cuotaParaAñadir) throws SQLException {
		Connection conexion = DriverManager.getConnection("jdbc:ucanaccess://BD\\IPSCentro1.mdb");
		Statement sentencia = conexion.createStatement(); 
		ResultSet rs =  sentencia.executeQuery("SELECT * FROM SOCIOS WHERE Id = " + idUsuario );
		ResultSetMetaData rsmd = rs.getMetaData();
		int columnsNumber = rsmd.getColumnCount();
		
		while (rs.next()) {
			for (int i = 1; i <= columnsNumber; i++) {
				String columnName = rsmd.getColumnName(i);
				String columnValue = rs.getString(i);
				
				if (columnName.equals("IdCuota")){
					ResultSet rsCuotas = sentencia.executeQuery("SELECT * FROM TarifasSocios WHERE IdCuota = " + columnValue);
					ResultSetMetaData rsmdCuotas = rsCuotas.getMetaData();
					int columnsNumberTarifas = rsmdCuotas.getColumnCount();
					
					while(rsCuotas.next()){
						for (int j = 1; j <= columnsNumberTarifas; j++){
							String columnNameTarifas = rsmdCuotas.getColumnName(j);
							String columnValueTarifas = rsCuotas.getString(j);
							
							if (columnNameTarifas.equals("ImporteMensual")){
								@SuppressWarnings("unused")
								int rsFinal =  sentencia.executeUpdate("UPDATE TarifasSocios SET ImporteMensual =  ImporteMensual + " + cuotaParaAñadir + " WHERE Id = " + columnValue);
							}
						}
					}
				}			
			}
			System.out.println();
		}
		conexion.close();
	}

}
