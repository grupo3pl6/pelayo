package logica;

import java.awt.HeadlessException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;

import conexion.Conexion;

public class ManagerReservas {
	
	private Conexion conexion;
	private ResultSet resultset;
	
	public ManagerReservas() throws SQLException {
		conexion = new Conexion();
		resultset = null;
	}
	
	///////////////////////////////////////// Reservas ////////////////////////////////////////
	
	public DefaultComboBoxModel cargarCmbInstalaciones(DefaultComboBoxModel cmb) throws SQLException
	{
		String sql ="select * from Instalaciones";
		resultset = conexion.ejecutarConsulta(sql);
		try {
			while (resultset.next())
			{
				//String valor = resultset.getString("NombreInstalacion");

				cmb.addElement(new Instalacion(Integer.parseInt(resultset.getString("Id")), 
						resultset.getString("NombreInstalacion"), 
						Integer.parseInt(resultset.getString("IdCentro"))));

			}
			//conexion.cerrarConexion();
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		resultset.close();
		
		return cmb;
	}
	
	
	public DefaultComboBoxModel cargarCmbSocios(DefaultComboBoxModel cmb) throws SQLException
	{
		resultset = conexion.ejecutarConsulta("select * from Socios");
		try {
			while (resultset.next())
			{
				cmb.addElement(new Socio(Integer.parseInt(resultset.getString("Id")), resultset.getString("NombreSocio"), 
						resultset.getString("ApellidoSocio"), Integer.parseInt(resultset.getString("Idcentro")), 
						resultset.getString("DNI"), resultset.getString("FechaAlta"), resultset.getString("FechaBaja")
						));

			}
			//conexion.cerrarConexion();
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		resultset.close();
		
		return cmb;
	}
	
	
	public void hacerReservaCentro(Instalacion instalacion, Date fecha) throws SQLException, ParseException
	{
		String f = convertirFechaAstring(fecha);
		String sql = "insert into Reservas (FechaReserva, IdInstalacion, IdCliente, TipoReserva, DiaCompleto, Cancelada) values ('"+f+"' ,"+instalacion.getId()+",1, 'Centro', true, false);";
		conexion.insertarEnTabla(sql);
	}
	
	public void hacerReservaCentro(Instalacion instalacion, Date fecha, String desde, String hasta) throws SQLException, ParseException
	{
		String f = convertirFechaAstring(fecha);
		String sql = "insert into Reservas (FechaReserva, IdInstalacion, IdCliente, TipoReserva, Desde, Hasta, Cancelada) values ('"+f+"' ,"+instalacion.getId()+",1, 'Centro', '" + desde + "', '" + hasta + "', false);";
		conexion.insertarEnTabla(sql);
	}
	
	public void hacerReservaSocio(Instalacion instalacion, Socio socio, Date fecha, String desde, String hasta, String formaPago) throws SQLException, ParseException
	{
		String f = convertirFechaAstring(fecha);
		String sql = "insert into Reservas (FechaReserva, IdInstalacion, IdCliente, TipoReserva, Desde, Hasta, Cancelada, FormaPago) values ('"+f+
				"' ,"+instalacion.getId()+", " + socio.getId() + ", 'Socio', '" + desde + "', '" + hasta + "', false, '" + formaPago + "');";
		conexion.insertarEnTabla(sql);
		//obtener IdReserva que acabo de insertar
		
		sql = "select max(id) as IdMax from Reservas";
		resultset = conexion.ejecutarConsulta(sql);
		int idReserva = -1;
		while (resultset.next())
			idReserva = Integer.parseInt(resultset.getString("IdMax"));
		
		//obtener importe de la reserva
		sql = "select ImporteHora from TarifasInstalaciones where IdInstalacion = " + instalacion.getId() + " and Vigente = true";
		resultset = conexion.ejecutarConsulta(sql);
		int importe = -1;
		while (resultset.next())
			importe = (Integer.parseInt(hasta) - Integer.parseInt(desde)) * Integer.parseInt(resultset.getString("ImporteHora"));
		// insertar Pago
		
		sql = "insert into Pagos (Usuario, Importe, Tipo, IdReserva) values (" + socio.getId() + 
				", " + importe + ", '" + formaPago + "', " + idReserva + ");";
		conexion.insertarEnTabla(sql);
		
	}
	
	public Date convertirFecha (Object fecha)//NO USAR
	{
		SimpleDateFormat fdia = new SimpleDateFormat("dd");
		int dia = Integer.parseInt(fdia.format(fecha));
		SimpleDateFormat fmes = new SimpleDateFormat("MM");
		int mes = Integer.parseInt(fmes.format(fecha));
		SimpleDateFormat fanio = new SimpleDateFormat("yyyy");
		int anio = Integer.parseInt(fanio.format(fecha));
		Date fecha2 = new Date (dia, mes, anio);
		
		return fecha2;
	}
	
	
	public String convertirFechaAstring(Date fecha) throws ParseException
	{
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");  
		String fechaConFormato = sdf.format(fecha);
		return fechaConFormato;
		
	}
	
	public Date convertirStringAfecha (String cadena) throws ParseException
	{
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");  
		Date nuevaFecha = (Date) sdf.parse(cadena);  
		return nuevaFecha;
	}
	
	public boolean instalacionReservadaDia (Instalacion instalacion, Date fecha) throws ParseException, SQLException
	{
		String f = convertirFechaAstring(fecha);
		String sql = "select * from Reservas where IdInstalacion = " + instalacion.getId() + 
				" and FechaReserva = '" + f + "' and DiaCompleto = true";
		resultset = conexion.ejecutarConsulta(sql);
		if (!resultset.next())
		{	
			//System.out.println("no esta reservada por el centro como dia completo");
			resultset.close();
			return false;
		}
		else
		{
			//System.out.print("reservada por el centro como dia completo");
			JOptionPane.showMessageDialog(null,
                    "Error: La instalacion esta reservada por el centro durante todo el dia", "Error Message",
                    JOptionPane.ERROR_MESSAGE);
			//txtHasta.requestFocus();
			resultset.close();
			return true;
		}
		
	}
	
	public boolean instalacionReservadaHoras (Instalacion instalacion, Date fecha, String desde, String hasta) throws ParseException, SQLException
	{
		String f = convertirFechaAstring(fecha);
		//mirar si la instalacion esta reservada por horas
		int desdeH = Integer.parseInt(desde);
		int hastaH = Integer.parseInt(hasta);
		String sql = "select * from Reservas where IdInstalacion = " + instalacion.getId() + 
				" and FechaReserva = '" + f + "' and DiaCompleto = false";
		resultset = conexion.ejecutarConsulta(sql);
		while (resultset.next())
		{
			int desdeHbd = Integer.parseInt((resultset.getString("Desde")));
			int hastaHbd = Integer.parseInt((resultset.getString("Hasta")));

			if ((desdeH >= desdeHbd && desdeH < hastaHbd) || (hastaH > desdeHbd && hastaH <= hastaHbd))
			{
				//System.out.println("la instalacion esta reservada en esas horas (installReservdaHoras)");
				JOptionPane.showMessageDialog(null,
	                    "Error: La instalacion esta reservada en esas horas", "Error Message",
	                    JOptionPane.ERROR_MESSAGE);
				//txtHasta.requestFocus();
				
				resultset.close();
				return true;
			}
		}
		return false;
	}
	
	public boolean reservaSimultaneaSocio (Socio socio, Date fecha, String desde, String hasta) throws ParseException, SQLException
	{
		String f = convertirFechaAstring(fecha);
		int desdeH = Integer.parseInt(desde);
		int hastaH = Integer.parseInt(hasta);
		String sql = "select * from Reservas where IdCliente = " + socio.getId() + 
				" and FechaReserva = '" + f + "' and DiaCompleto = false and TipoReserva = 'Socio'";
		resultset = conexion.ejecutarConsulta(sql);
		while (resultset.next())
		{
			int desdeHbd = Integer.parseInt((resultset.getString("Desde")));
			int hastaHbd = Integer.parseInt((resultset.getString("Hasta")));

			if ((desdeH >= desdeHbd && desdeH < hastaHbd) || (hastaH > desdeHbd && hastaH <= hastaHbd))
			{
				//System.out.println("el socion tiene una reserva simultanea");
				JOptionPane.showMessageDialog(null,
	                    "Error: El socio tiene una reserva simultanea", "Error Message",
	                    JOptionPane.ERROR_MESSAGE);
				//txtHasta.requestFocus();
				resultset.close();
				return true;
			}
		}
		return false;
	}
	
	public boolean fechaReservaSocioCorrecta(Date fecha)
	{
		//maximo 15 dias de antelacion y hasta 1 hora antes de su inicio
		Calendar fechaActualC = Calendar.getInstance();
		Date fechaActual=fechaActualC.getTime(); 
		
		SimpleDateFormat sdfdias = new SimpleDateFormat("dd");  
		String fDiasReserva = sdfdias.format(fecha);
		String fDiasActual = sdfdias.format(fechaActual);
		SimpleDateFormat sdfmes = new SimpleDateFormat("MM");  
		String fMesReserva = sdfmes.format(fecha);
		String fMesActual = sdfmes.format(fechaActual);
		SimpleDateFormat sdfAnio = new SimpleDateFormat("yyyy");  
		String fAnioReserva = sdfAnio.format(fecha);
		String fAnioActual = sdfAnio.format(fechaActual);
		
		long diasEntreFechas; //reserva - actual
		diasEntreFechas = difDiasEntre2fechas (Integer.parseInt(fAnioReserva), Integer.parseInt(fMesReserva), Integer.parseInt(fDiasReserva), 
				Integer.parseInt(fAnioActual), Integer.parseInt(fMesActual), Integer.parseInt(fDiasActual));
		if (diasEntreFechas <= 15)	
				return true;		
		else
		{
			JOptionPane.showMessageDialog(null,
                    "Error: La reserva no se puede hacer con antelacion superior a 15 dias", "Error Message",
                    JOptionPane.ERROR_MESSAGE);
			//txtHasta.requestFocus();
			return false;
		}
	}
	
	public boolean fechaReservaSocioCorrecta(Date fecha, String desde)
	{
		//maximo 15 dias de antelacion y hasta 1 hora antes de su inicio (para AccsociosReservas)
		Calendar fechaActualC = Calendar.getInstance();
		Date fechaActual=fechaActualC.getTime(); 
		
		SimpleDateFormat sdfdias = new SimpleDateFormat("dd");  
		String fDiasReserva = sdfdias.format(fecha);
		String fDiasActual = sdfdias.format(fechaActual);
		SimpleDateFormat sdfmes = new SimpleDateFormat("MM");  
		String fMesReserva = sdfmes.format(fecha);
		String fMesActual = sdfmes.format(fechaActual);
		SimpleDateFormat sdfAnio = new SimpleDateFormat("yyyy");  
		String fAnioReserva = sdfAnio.format(fecha);
		String fAnioActual = sdfAnio.format(fechaActual);
		
		long diasEntreFechas; //reserva - actual
		diasEntreFechas = difDiasEntre2fechas (Integer.parseInt(fAnioReserva), Integer.parseInt(fMesReserva), Integer.parseInt(fDiasReserva), 
				Integer.parseInt(fAnioActual), Integer.parseInt(fMesActual), Integer.parseInt(fDiasActual));
		if (diasEntreFechas < 15)	
		{
			if (diasEntreFechas == 0)
				if (horaCorrecta(desde, fechaActual))
					return true;
				else
					return false;
			else
				return true;
			
		}	
		else
		{
			JOptionPane.showMessageDialog(null,
                    "Error: La reserva no se puede hacer con antelacion superior a 15 dias", "Error Message",
                    JOptionPane.ERROR_MESSAGE);
			//txtHasta.requestFocus();
			return false;
		}
	}
	
	public boolean horaCorrecta(String desde, Date fechaActual)
	{
		
		int ff = fechaActual.getHours();
		int min = fechaActual.getMinutes();
		int restar = 1; //horas a restar
		
		if (min > 0)
			restar += 1;
		if (ff <= (Integer.parseInt(desde) - restar))
			return true;
		//System.out.println("no se puede reservar, solo hasta 1 h antes de su inicio");
		JOptionPane.showMessageDialog(null,
                "Error: No se admiten reservas cuando falta menos de 1h para el inicio", "Error Message",
                JOptionPane.ERROR_MESSAGE);
		//txtHasta.requestFocus();
		return false;
			
				
	}
	
	 public long difDiasEntre2fechas(int Y1,int M1,int D1,int Y2,int M2,int D2){
		 java.util.GregorianCalendar date1=new java.util.GregorianCalendar(Y1,M1,D1);
		 java.util.GregorianCalendar date2=new java.util.GregorianCalendar(Y2,M2,D2);
		 long difms=date1.getTimeInMillis() - date2.getTimeInMillis();
		 long difd=difms / (1000 * 60 * 60 * 24);
		 return difd;
		 }
	 
	 public boolean bajaMesEnCurso(String f) throws ParseException
	 {
		 if (f != null){
		 Date fecha;
		 fecha = convertirStringAfecha(f);
		 SimpleDateFormat sdfmes = new SimpleDateFormat("MM");  

		 Calendar fechaActualC = Calendar.getInstance();
		 Date fechaActual=fechaActualC.getTime(); 

		 String fMesBaja = sdfmes.format(fecha);
		 String fMesActual = sdfmes.format(fechaActual);
		 if (fMesBaja.equals(fMesActual))
		 {
			 JOptionPane.showMessageDialog(null,
						"Error: La fecha de baja es para el mes en curso, no se pueden hacer reservas", "Error Message",
						JOptionPane.ERROR_MESSAGE);
			 return true;
		 }
		 }

		 return false;
	 }
	 
	 public boolean fechaPosteriorAactual(Date fecha)
	 {
		 Calendar fechaActualC = Calendar.getInstance();
		 Date fechaActual=fechaActualC.getTime(); 

		 SimpleDateFormat sdfdias = new SimpleDateFormat("dd");  
		 String fDiasReserva = sdfdias.format(fecha);
		 String fDiasActual = sdfdias.format(fechaActual);
		 SimpleDateFormat sdfmes = new SimpleDateFormat("MM");  
		 String fMesReserva = sdfmes.format(fecha);
		 String fMesActual = sdfmes.format(fechaActual);
		 SimpleDateFormat sdfAnio = new SimpleDateFormat("yyyy");  
		 String fAnioReserva = sdfAnio.format(fecha);
		 String fAnioActual = sdfAnio.format(fechaActual);

		 long diasEntreFechas; //fecha - actual
		 diasEntreFechas = difDiasEntre2fechas (Integer.parseInt(fAnioReserva), Integer.parseInt(fMesReserva), Integer.parseInt(fDiasReserva), 
				 Integer.parseInt(fAnioActual), Integer.parseInt(fMesActual), Integer.parseInt(fDiasActual));
		 if (diasEntreFechas >= 0)
			 return true;
		 else
		 {JOptionPane.showMessageDialog(null,
	                "Error: No se pueden hacer reservas en una fecha pasada", "Error Message",
	                JOptionPane.ERROR_MESSAGE);
			 return false;
		 }
	 }
	 
	 ///////////////// Validacion /////////////////////
	 
	 public Socio validarUsuario(String usuario, char[] pass) throws SQLException
	 {
		 String passw = String.valueOf(pass);
		 String sql = "select count(*) as size from socios where Usuario = '" + usuario + "' and Pass = '" + passw +"';";
		 resultset = conexion.ejecutarConsulta(sql);
		 if (resultset.getString("size").equals("1"))
		 {
			 sql = "select * from socios where Usuario = '" + usuario + "' and Pass = '" + passw +"';";
			 resultset = conexion.ejecutarConsulta(sql);
			 return new Socio(Integer.parseInt(resultset.getString("Id")), resultset.getString("NombreSocio"), 
						resultset.getString("ApellidoSocio"), Integer.parseInt(resultset.getString("Idcentro")), 
						resultset.getString("DNI"), resultset.getString("FechaAlta"), resultset.getString("FechaBaja")
						);
		 }
		 return null;
	 }
			 
		 
	 

}
