package logica;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import conexion.Conexion;

public class ManagerConsultas {	
	
	public ResultSet nombreInstalaciones(Conexion conexion) {
		try {
			String consulta = "select distinct nombreInstalacion from Instalaciones";
			ResultSet rs = conexion.ejecutarConsulta(consulta);
			return rs;
		} catch (SQLException e) {
			System.out.println("Error de consulta");
			e.printStackTrace();
		}
		return null;
	}
	
	public ResultSet horasInstalacionReservadas(Conexion conexion, String instalacion, String fecha){
		try {
			String consulta = "select Id, FechaReserva, IdInstalacion,"
					+ " IdCliente, TipoReserva, Ocupada, HoraSalida, Desde,"
					+ " Hasta, DiaCompleto, Pagada, Cancelada, FormaPago,"
					+ " HoraLlegada from Reservas where FechaReserva like '" + fecha + "' and"
					+ " IdInstalacion in (select Id from Instalaciones where NombreInstalacion='" + instalacion + "') ORDER BY"
					+ " Desde asc";
			ResultSet rs = conexion.ejecutarConsulta(consulta);
			return rs;
		} catch (SQLException e) {
			System.out.println("Error de consultaaaaaaaaaaaaa");
			e.printStackTrace();
		}
		return null;
	}
	
	public ResultSet horas(Conexion conexion, int idReserva){
		try {
			String consulta = "select Desde, Hasta, FechaReserva from Reservas WHERE Id= " + idReserva;
			ResultSet rs = conexion.ejecutarConsulta(consulta);
			return rs;
		} catch (SQLException e) {
			System.out.println("Error al extraer las horas");
			e.printStackTrace();
		}
		return null;
	}
	
	public int actualizarHoraLlegada(int id, String horaLlegada){
		try {
			Connection conexion = DriverManager.getConnection("jdbc:ucanaccess://BD\\IPSCentro1.mdb");
			Statement sentencia = conexion.createStatement();
			sentencia.executeUpdate("UPDATE RESERVAS SET HoraLlegada = '"+horaLlegada+"' WHERE Id = " + id);
			conexion.close();
			return 1;
		} catch (SQLException e) {
			System.out.println("Error al actualizar hora de llegada");
			e.printStackTrace();
		}
		return -1;
	}
	
	public int actualizarHoraSalida(int id, String horaSalida){
		try {
			Connection conexion = DriverManager.getConnection("jdbc:ucanaccess://BD\\IPSCentro1.mdb");
			Statement sentencia = conexion.createStatement();
			sentencia.executeUpdate("UPDATE RESERVAS SET HoraSalida = '"+horaSalida+"' WHERE Id = " + id);
			conexion.close();
			return 1;
		} catch (SQLException e) {
			System.out.println("Error al actualizar hora de salida");
			e.printStackTrace();
		}
		return -1;
	}
	
	public boolean cancelarReserva(int idReserva){
		Connection conexion;
		try {
			conexion = DriverManager.getConnection("jdbc:ucanaccess://BD\\IPSCentro1.mdb");
			Statement sentencia = conexion.createStatement();
			sentencia.executeUpdate("DELETE FROM Reservas WHERE Id = " + idReserva);
			conexion.close();
			return true;
		} catch (SQLException e) {
			System.out.println("Error al eliminar consulta");
			e.printStackTrace();
		}
		return false;
	}
}
