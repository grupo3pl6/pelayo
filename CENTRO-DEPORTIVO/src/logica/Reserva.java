package logica;

public class Reserva {
	
	private int Id;
	private String fechaReserva;
	private int IdInstalacion;
	private int IdCliente;
	private String TipoReserva;
	private boolean ocupada;
	private String horaSalida;
	private String desde;
	private String hasta;
	private boolean diaCompleto;
	private boolean pagada;
	private boolean cancelada;
	private String formaPago;
	private String horaLlegada;
	

	
	public Reserva(int id, String fechaReserva, int idInstalacion, int idCliente, String tipoReserva, boolean ocupada,
			String horaSalida, String desde, String hasta, boolean diaCompleto, boolean pagada, boolean cancelada,
			String formaPago, String horaLlegada) {
		super();
		Id = id;
		this.fechaReserva = fechaReserva;
		IdInstalacion = idInstalacion;
		IdCliente = idCliente;
		TipoReserva = tipoReserva;
		this.ocupada = ocupada;
		this.horaSalida = horaSalida;
		this.desde = desde;
		this.hasta = hasta;
		this.diaCompleto = diaCompleto;
		this.pagada = pagada;
		this.cancelada = cancelada;
		this.formaPago = formaPago;
		this.horaLlegada = horaLlegada;
	}
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getTipoReserva() {
		return TipoReserva;
	}
	public void setTipoReserva(String fecha) {
		this.TipoReserva = fecha;
	}

	public String getHasta() {
		return hasta;
	}
	public void setHasta(String hasta) {
		this.hasta = hasta;
	}
	public boolean isDiaCompleto() {
		return diaCompleto;
	}
	public void setDiaCompleto(boolean diaCompleto) {
		this.diaCompleto = diaCompleto;
	}
	public boolean isPagada() {
		return pagada;
	}
	public void setPagada(boolean pagada) {
		this.pagada = pagada;
	}
	public boolean isCancelada() {
		return cancelada;
	}
	public void setCancelada(boolean cancelada) {
		this.cancelada = cancelada;
	}
	public String getFormaPago() {
		return formaPago;
	}
	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}
	public String getHoraLlegada() {
		return horaLlegada;
	}
	public void setHoraLlegada(String horaLlegada) {
		this.horaLlegada = horaLlegada;
	}
	public String getFechaReserva() {
		return fechaReserva;
	}

	public void setFechaReserva(String fechaReserva) {
		this.fechaReserva = fechaReserva;
	}

	public int getIdInstalacion() {
		return IdInstalacion;
	}

	public void setIdInstalacion(int idInstalacion) {
		IdInstalacion = idInstalacion;
	}

	public int getIdCliente() {
		return IdCliente;
	}

	public void setIdCliente(int idCliente) {
		IdCliente = idCliente;
	}

	public boolean isOcupada() {
		return ocupada;
	}

	public void setOcupada(boolean ocupada) {
		this.ocupada = ocupada;
	}

	public String getHoraSalida() {
		return horaSalida;
	}

	public void setHoraSalida(String horaSalida) {
		this.horaSalida = horaSalida;
	}
	public String getDesde() {
		return desde;
	}
	public void setDesde(String desde) {
		this.desde = desde;
	}
}
