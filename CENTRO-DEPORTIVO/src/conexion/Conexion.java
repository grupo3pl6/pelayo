package conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Conexion {
	
	private Connection conexion;
	private Statement sentencia;
	private ResultSet resultset;
	
	public Conexion() throws SQLException{
			conexion = DriverManager.getConnection("jdbc:ucanaccess://BD\\IPSCentro1.mdb");
	        sentencia = conexion.createStatement();
	        //PRUEBA CAMBIO
	}
	
	public void cerrarConexion() throws SQLException{
		resultset.close();
		sentencia.close();
		conexion.close();	
	}
	
	public ResultSet ejecutarConsulta(String consulta) throws SQLException{
		resultset = sentencia.executeQuery(consulta);
		return resultset;
	}
	
	public void insertarEnTabla(String sql) throws SQLException{
		sentencia.executeUpdate(sql);
	}

}
